import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AppConfigService } from '../utils/config/app-config.service';
import { Rol } from '../models/rol';

/**
  * Clase representativa del servicio de roles, como contenedora de los métodos relacionados
  * a la gestión de operaciones en base a roles asociados a los usuarios
  */
@Injectable({
  providedIn: 'root'
})
export class RolService {

  /**
    * Método constructor de la clase
    */
  constructor(
    private http: HttpClient,
    private router: Router,
    private appConfigService: AppConfigService
  ) { }

  /**
    * Método que permite realizar la búsqueda de la dirección URL asociada a la seguridad de
    * usuarios, con el uso del servicio para configuración de la aplicación
    * @returns objeto de tipo String con la dirección URL, nulo en caso contrario
    */
  private getEndPoint(): string {
    return this.appConfigService.getConfig("URL_AUTH_API") + '/api/security/roles';
  }

  /**
    * Método que permite realizar la búsqueda de la lista de roles, con el uso de una petición web
    * a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición
    */
  getList(): Observable<Rol[]> {
    return this.http.get<Rol[]>(this.getEndPoint());
  }

  /**
    * Método que permite realizar la búsqueda de información relacionada a los roles de
    * usuarios, a partir del número de página, columna y ordenamiento obtenidos como parámetros,
    * con el uso de una petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  getListPaginated(pagina: number, col: string, sort: string): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/page/${pagina}/${col}/${sort}`).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la creación de un rol a asociar en conjunto con un usuario
    * de la aplicación, a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  create(rol: Rol): Observable<any> {
    return this.http.post<any>(`${this.getEndPoint()}/create`, rol).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la eliminación de un rol a asociar en conjunto con un usuario
    * de la aplicación, a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  delete(idRol: number): Observable<any> {
    return this.http.delete<any>(`${this.getEndPoint()}/delete/${idRol}`).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite realizar la búsqueda de información relacionada a un rol, a partir 
    * de su identificador obtenido como parámetro, con el uso de una petición web
    * a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  getById(idRol: number): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/edit/${idRol}`).pipe(
      catchError(e => {
        this.router.navigate(['/seguridad/roles']);

        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la actualización de un rol a asociar en conjunto con un usuario
    * de la aplicación, a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  update(rol: Rol): Observable<any> {
    return this.http.put<any>(`${this.getEndPoint()}/update/${rol.id}`, rol).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la activación de un rol a asociar en conjunto con un usuario
    * de la aplicación, a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  activate(idRol: number): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/activate/${idRol}`).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la desactivación de un rol a asociar en conjunto con un usuario
    * de la aplicación, a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  desactivate(idRol: number): Observable<any> {
    return this.http.put<any>(`${this.getEndPoint()}/desactivate/${idRol}`,idRol).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

}
