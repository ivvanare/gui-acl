import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CorreoMensajes } from '../models/correo';
import { AppConfigService } from '../utils/config/app-config.service';


@Injectable({
  providedIn: 'root'
})
export class CorreoService {

  constructor(
    private httpClient: HttpClient,
    private appConfigService: AppConfigService
  ) {}

    /**
    * Método que permite realizar la búsqueda de la dirección URL asociada a la gestión
    * de contraseñas, con el uso del servicio para configuración de la aplicación
    * @returns objeto de tipo String con la dirección URL, nulo en caso contrario
    */
   private getEndPoint(): string {
    return this.appConfigService.getConfig("URL_BASE_API") + '/';
  }

  save(correo: CorreoMensajes){
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.httpClient.post(`${this.getEndPoint()}correos`, correo, {headers: headers});
  }

}

