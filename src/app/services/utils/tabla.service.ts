import { Injectable } from '@angular/core';

/**
  * Clase representativa del servicio para tablas en relación a las tablas de información
  * representadas en la solución
  */
@Injectable({
  providedIn: 'root'
})
export class TablaService {

  path: string[];

  order: number = 1;

  /**
    * Método constructor de la clase
    */
  constructor() { }

  /**
    * Método que permite realizar el control de seguimiento para los elementos de una tabla,
    * desplegados a través de un ciclo for
    */
  trackBy(index, item) {
    if (!item) { return null; }
    return index;
  }

  /**
    * Método que permite establecer los parámetros de ordenamiento en el servicio, referenciados
    * en el campo y la dirección, a partir del campo seleccionado y obtenido como parámetro
    */
  sortTable(prop: string) {
    this.path = [prop];
    this.order = this.order * (-1);
    return false;
  }

  /**
    * Método que permite ejecutar el ordenamiento de los datos en una tabla, a partir de
    * un campo y la dirección de orden a establecer
    */
  doSortTable(array: any, campo: string, sort: string): any[] {
    return array.sort((obj1, obj2) => {
      if (obj1[campo] > obj2[campo]) {
        return sort == 'asc' ? 1 : -1;
      }
      if (obj1[campo] < obj2[campo]) {
        return sort == 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  /**
    * Método que permite la búsqueda del ícono relacionado a la dirección del ordenamiento
    * en la tabla, a partir del campo seleccionado y obtenido como parámetro
    */
  getIcon(prop: string): string {
    var iconClass = "swap-outline";

    if (this.path != null && this.path.indexOf(prop) != -1) {
      if (this.order === -1) {
        iconClass = 'arrow-down-outline';
      } else {
        iconClass = 'arrow-up-outline';
      }
    }

    return iconClass;
  }

  /**
    * Método que permite obtener el valor textual del campo seleccionado para el
    * ordenamiento de la tabla
    */
  getPathValue(): string | undefined {
    return this.path != undefined ? this.path[0] : undefined;
  }

  /**
    * Método que permite obtener el valor textual de la dirección del ordenamiento
    * en la tabla, a partir del campo seleccionado y obtenido como parámetro
    */
  getSort(prop: string): string {
    var orderString = "asc";

    if (this.path != null && this.path.indexOf(prop) != -1) {
      if (this.order === -1) {
        orderString = 'desc';
      }
    }

    return orderString;
  }

}
