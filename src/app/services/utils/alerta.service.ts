import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

/**
  * Clase representativa del servicio para alertas en relación a los mensajes de alertas
  * tomando en cuenta las acciones a realizar en la aplicación
  */
@Injectable({
  providedIn: 'root'
})

export class AlertaService {

  /*
  icon_Confirm:string = `<img style="width: 20px;" src="assets/icons/checkmark-outline.svg">`;
  icon_Cancel:string = '<img style="width: 20px;" src="assets/icons/close-outline.svg">';
  */
  icon_Confirm:string = '';
  icon_Cancel:string = '';
  
  private readonly SWAL_TOAST = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  });

  private readonly SWAL_QUESTION = Swal.mixin({
    icon: 'question',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: `${this.icon_Confirm} Si`,
    cancelButtonText: `${this.icon_Cancel} No`
  });

  private readonly SWAL_ERROR = Swal.mixin({
    icon: 'error',
    confirmButtonColor: '#d33',
    confirmButtonText: `${this.icon_Confirm} Ok`,
  });

  private readonly SWAL_SUCCESS = Swal.mixin({
    icon: 'success',
    confirmButtonColor: '#3085d6',
    confirmButtonText: `${this.icon_Confirm} Ok`,
  });

  private readonly SWAL_INFO = Swal.mixin({
    icon: 'info',
    confirmButtonColor: '#3085d6',
    confirmButtonText: `${this.icon_Confirm} Ok`,
  });

  private readonly SWAL_WARN = Swal.mixin({
    icon: 'warning',
    confirmButtonColor: '#3085d6',
    confirmButtonText: `${this.icon_Confirm} Ok`,
  });

  private readonly SWAL_TOAST_CONFIRM = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: true,
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: `${this.icon_Confirm} Si`,
    cancelButtonText: `${this.icon_Cancel} No`
  });

  private readonly SWAL_SELECT = Swal.mixin({
    input: 'select',
    inputPlaceholder: 'Selecione un valor',
    showConfirmButton: true,
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: `${this.icon_Confirm} Ok`,
    cancelButtonText: `${this.icon_Cancel} Cancelar`
  });

  /**
    * Método constructor de la clase
    */
  constructor() { }

  /**
    * Método que permite el despliegue de una alerta de tipo toast en la aplicación
    */
  doSwalToast(properties: any): void {
    this.SWAL_TOAST.fire(properties);
  }

  /**
    * Método que permite el despliegue de una alerta de tipo toast con confirmación del
    * usuario en la aplicación
    * @returns objeto de tipo Any con las propiedades de la alerta
    */
  doSwalToastConfirm(): any {
    return this.SWAL_TOAST_CONFIRM;
  }

  /**
    * Método que permite el despliegue de una alerta de tipo question en la aplicación
    * @returns objeto de tipo Any con las propiedades de la alerta
    */
  doSwalQuestion(): any {
    return this.SWAL_QUESTION;
  }

  /**
    * Método que permite el despliegue de una alerta con elementos de selección en la aplicación
    * @returns objeto de tipo Any con las propiedades de la alerta
    */
  doSwalSelect(): any {
    return this.SWAL_SELECT;
  }

  /**
    * Método que permite el despliegue de una alerta de tipo error en la aplicación
    */
  doSwalError(properties: any): void {
    this.SWAL_ERROR.fire(properties);
  }

  /**
    * Método que permite el despliegue de una alerta de tipo success en la aplicación
    */
  doSwalSuccess(properties: any): void {
    this.SWAL_SUCCESS.fire(properties);
  }

  /**
    * Método que permite el despliegue de una alerta de tipo information en la aplicación
    */
  doSwalInformation(properties: any): void {
    this.SWAL_INFO.fire(properties);
  }

  /**
    * Método que permite el despliegue de una alerta de tipo warning en la aplicación
    */
  doSwalWarning(properties: any): void {
    this.SWAL_WARN.fire(properties);
  }

}
