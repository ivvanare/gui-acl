import { Injectable } from '@angular/core';

/**
  * Clase representativa del servicio para ventanas de modal en relación a los
  * requerimientos de procesos en la solución
  */
@Injectable({
  providedIn: 'root'
})
export class ModalService {

  modal: boolean = false;

  /**
    * Método constructor de la clase
    */
  constructor() { }

  /**
    * Método que permite establecer la propiedad de apertura para la ventana de tipo modal
    */
  abrirModal() {
    this.modal = true;
  }

  /**
    * Método que permite establecer la propiedad de cierre para la ventana de tipo modal
    */
  cerrarModal() {
    this.modal = false;
  }


}
