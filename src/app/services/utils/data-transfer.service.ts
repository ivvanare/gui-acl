import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

/**
  * Clase utilitaria representativa del servicio para transferencia de información en relación a los
  * datos utilizados en la comunicación entre componentes
  */
@Injectable({
  providedIn: 'root'
})
export class DataTransferService {

  private messageSource = new BehaviorSubject('0');
  currentMessage = this.messageSource.asObservable();

  private dateRangeSource = new BehaviorSubject(null);
  currentDateRange = this.dateRangeSource.asObservable();

  private datePopupSource = new BehaviorSubject(null);
  currentDatePopup = this.datePopupSource.asObservable();

  private userDataSource = new BehaviorSubject(null);
  currentUserData = this.userDataSource.asObservable();

  /**
    * Método constructor de la clase
    */
  constructor() { }

  /**
    * Método que permite realizar el ajuste del mensaje para los tiempos de carga de la
    * plantilla visual, a partir del mensaje obtenido como parámetro
    */
  changeMessage(message: string) {
    this.messageSource.next(message)
  }

  /**
    * Método que permite realizar el ajuste de las fechas seleccionadas en un rango, dentro un select,
    * a partir de los elementos de fechas obtenidos como parámetros
    */
  changeDateRange(message: any) {
    this.dateRangeSource.next(message)
  }

  /**
    * Método que permite realizar el ajuste de la fecha seleccionada en una ventana desplegable, dentro un select,
    * a partir de los elementos de la fecha obtenidos como parámetros
    */
  changeDatePopup(message: any) {
    this.datePopupSource.next(message)
  }

  /**
    * Método que permite realizar el ajuste de la información relacionada a un usuario, a partir
    * de los datos del mismo obtenidos como parámetros
    */
  changeUserInfo(message: any) {
    this.userDataSource.next(message)
  }

}
