import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

/**
  * Clase representativa del servicio para el icono de carga en relación a los
  * tiempos de carga de la plantilla visual
  */
@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  isLoading = new Subject<boolean>();

  /**
    * Método constructor de la clase
    */
  constructor() { }

  /**
    * Método que permite ajustar las propiedades del icono de carga para visualizarlo en la
    * plantilla visual
    */
  show() {
    this.isLoading.next(true);
  }

  /**
    * Método que permite ajustar las propiedades del icono de carga para ocultarlo de la
    * plantilla visual
    */
  hide() {
    this.isLoading.next(false);
  }

}
