import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConstantesConfig } from 'src/app/utils/constant/config';
import { Contrasena } from '../../models/contrasena';
import { ContrasenaService } from '../contrasena.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

/**
  * Clase representativa del servicio para validaciones de formularios, como contenedora de los 
  * métodos relacionados a la gestión de validaciones en los formularios de la aplicación
  */
@Injectable({
  providedIn: 'root'
})
export class FormValidatorService {

  contrasenaValidators: Contrasena[];

  private obsParam$: Subject<void> = new Subject<void>();

  /**
    * Método constructor de la clase
    */
  constructor(
    private formBuilder: FormBuilder,
    private contrasenaService: ContrasenaService
  ) { }

  /**
    * Método que permite la comparación de valores entre dos campos de un formulario en
    * específico, a partir de los mismos obtenidos por parámetros
    */
  private MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustmatch) {
        return;
      }

      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustmatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }

  /**
    * Método que permite la comparación de valores entre dos campos de un formulario en
    * específico, a partir de un valor dependiente dado que permita el cumplimiento de
    * una condición. Los valores son obtenidos como parámetros
    */
  private DependsOn(controlName: string, dependentControlName: string, dependentValue: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const dependentControl = formGroup.controls[dependentControlName];

      if (control.errors && !control.errors.dependson) {
        return;
      }

      if (dependentControl.value == dependentValue) {
        if (control.value == null || control.value == undefined || control.value == '') {
          control.setErrors({ dependson: true });
        } else {
          control.setErrors(null);
        }
      } else {
        control.setErrors(null);
      }
    }
  }

  /**
    * Método que permite la comparación de valores entre tres campos de un formulario en
    * específico, a partir de dos valores dependientes dado que permita el cumplimiento de
    * una condición. Los valores son obtenidos como parámetros
    */
  private DependsOnTwoValues(controlName: string, dependentControlName1: string, dependentControlName2: string, dependentField: string, dependentValue: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const dependentControl1 = formGroup.controls[dependentControlName1];
      const dependentControl2 = formGroup.controls[dependentControlName2];

      if (control.errors && !control.errors.dependsontwovalues) {
        return;
      }

      if (control.value[dependentField] == dependentValue) {
        control.setErrors(null);
      } else {
        if ((dependentControl1.value == null || dependentControl1.value == undefined || dependentControl1.value == '') &&
          (dependentControl2.value == null || dependentControl2.value == undefined || dependentControl2.value == '')) {
          control.setErrors({ dependsontwovalues: true });
        } else {
          control.setErrors(null);
        }
      }
    }
  }

  /**
    * Método que permite la construcción del grupo de validaciones para los campos del formulario
    * de acceso al sistema
    * @returns objeto de tipo FormGroup con las validaciones de los campos
    */
  loginFormValidators(): FormGroup {
    return this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.minLength(2), Validators.maxLength(254)]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(254)]]
    });
  }

  /**
    * Método que permite la construcción del grupo de validaciones para los campos del formulario
    * de creación de un usuario
    * @returns objeto de tipo Promise con el objeto contenedor de las validaciones de los campos
    */
  createUserFormValidators(): Promise<FormGroup> {
    return new Promise(resolve => {
      this.contrasenaService.getList().pipe(
        takeUntil(this.obsParam$)
      ).subscribe(response => {
        this.contrasenaValidators = response as Contrasena[];
        this.contrasenaValidators = this.contrasenaValidators.filter((element: Contrasena) => element.status == 1);

        var arrayValidations: Array<any> = [];
        arrayValidations.push(Validators.required);

        this.contrasenaValidators.forEach(element => {
          if (element.regex == 0) {
            if (element.cod_requirement == ConstantesConfig.COD_CONTRASENA_MINLEGTH) {
              arrayValidations.push(Validators.minLength(Number(element.value)));
            } else if (element.cod_requirement == ConstantesConfig.COD_CONTRASENA_MAXLEGTH) {
              arrayValidations.push(Validators.maxLength(Number(element.value)));
            }
          } else {
            arrayValidations.push(Validators.pattern(element.value));
          }
        });

        resolve(this.formBuilder.group({
          email: ['', [Validators.required, Validators.email, Validators.minLength(2), Validators.maxLength(254)]],
          name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(254)]],
          roles: [[], Validators.required],
          password: ['', arrayValidations],
          password_confirmation: ['', Validators.required]
        },
          {
            validator: this.MustMatch('password', 'password_confirmation')
          }
        ))
      });
    });
  }

  /**
    * Método que permite la construcción del grupo de validaciones para los campos del formulario
    * de edición de un usuario
    * @returns objeto de tipo FormGroup con las validaciones de los campos
    */
  editUserFormValidators(): FormGroup {
    return this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.minLength(2), Validators.maxLength(254)]],
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(254)]],
      roles: [[], Validators.required]
    });
  }

  /**
    * Método que permite la construcción del grupo de validaciones para los campos del formulario
    * de roles
    * @returns objeto de tipo FormGroup con las validaciones de los campos
    */
  roleFormValidators(): FormGroup {
    return this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(254)]],
      all_permissions: ['0', [Validators.required, Validators.minLength(1), Validators.maxLength(1), Validators.pattern(ConstantesConfig.REGEX_BOOLEAN)]],
      permissions: [[]]
    },
      {
        validator: this.DependsOn('permissions', 'all_permissions', '0')
      }
    );
  }

  /**
    * Método que permite la construcción del grupo de validaciones para los campos del formulario
    * de creación de una institución
    * @returns objeto de tipo FormGroup con las validaciones de los campos
    */
  createInstFormValidators(): FormGroup {
    return this.formBuilder.group({
      
      waitSegSalida: [1, [Validators.required, Validators.min(1), Validators.pattern(ConstantesConfig.REGEX_SEGUNDOS)]],
      waitSegEntradaYc: [1, [Validators.required, Validators.min(1), Validators.pattern(ConstantesConfig.REGEX_SEGUNDOS)]],
      waitSegEntradaRcv: [1, [Validators.required, Validators.min(1), Validators.pattern(ConstantesConfig.REGEX_SEGUNDOS)]],
      passwordFtp: ['', [Validators.required]],
      passwordFtp2: ['', [Validators.required]]
    },
      {
        validator: this.MustMatch('passwordFtp', 'passwordFtp2')
      }
    );
  }

  /**
    * Método que permite la construcción del grupo de validaciones para los campos del formulario
    * de edición de una institución
    * @returns objeto de tipo FormGroup con las validaciones de los campos
    */
  editInstFormValidators(): FormGroup {
    return this.formBuilder.group({
      waitSegSalida: [1, [Validators.required, Validators.min(1), Validators.pattern(ConstantesConfig.REGEX_SEGUNDOS)]],
      waitSegEntradaYc: [1, [Validators.required, Validators.min(1), Validators.pattern(ConstantesConfig.REGEX_SEGUNDOS)]],
      waitSegEntradaRcv: [1, [Validators.required, Validators.min(1), Validators.pattern(ConstantesConfig.REGEX_SEGUNDOS)]],
      passwordFtp: [''],
      passwordFtp2: ['']
    },
      {
        validator: this.MustMatch('passwordFtp', 'passwordFtp2')
      }
    );
  }

  /**
    * Método que permite la construcción del grupo de validaciones para los campos del formulario
    * de rutas para una institución
    * @returns objeto de tipo FormGroup con las validaciones de los campos
    */
  instRutaFormValidators(): FormGroup {
    return this.formBuilder.group({
      detalleRuta: ['', Validators.required],
    });
  }

  /**
    * Método que permite la construcción del grupo de validaciones para los campos del formulario
    * de parámetros de la aplicación
    * @returns objeto de tipo FormGroup con las validaciones de los campos
    */
  paramFormValidators(): FormGroup {
    return this.formBuilder.group({
      ide: ['000', [Validators.required, Validators.minLength(3), Validators.maxLength(3), Validators.pattern(ConstantesConfig.REGEX_ID_PARAMETRO)]],
      descripcion: ['', Validators.required],
      valor: ['', Validators.required]
    });
  }

  /**
    * Método que permite la construcción del grupo de validaciones para los campos del formulario
    * de procesos o servicios
    * @returns objeto de tipo FormGroup con las validaciones de los campos
    */
  jobFormValidators(): FormGroup {
    return this.formBuilder.group({
      clasificacionJob: ['', Validators.required],
      descripcionJob: ['', Validators.required]
    });
  }

  /**
    * Método que permite la construcción del grupo de validaciones para los campos del formulario
    * de rutas
    * @returns objeto de tipo FormGroup con las validaciones de los campos
    */
  rutaFormValidators(): FormGroup {
    return this.formBuilder.group({
      codRuta: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern(ConstantesConfig.REGEX_COD_RUTA)]],
      descripcionRuta: ['', Validators.required]
    });
  }

  /**
    * Método que permite la construcción del grupo de validaciones para los campos del formulario
    * de cambio de contraseña para un usuario
    * @returns objeto de tipo Promise con el objeto contenedor de las validaciones de los campos
    */
  editPassUserFormValidators(): Promise<FormGroup> {
    return new Promise(resolve => {
      this.contrasenaService.getList().pipe(
        takeUntil(this.obsParam$)
      ).subscribe(response => {
        this.contrasenaValidators = response as Contrasena[];
        this.contrasenaValidators = this.contrasenaValidators.filter((element: Contrasena) => element.status == 1);

        var arrayValidations: Array<any> = [];
        arrayValidations.push(Validators.required);

        this.contrasenaValidators.forEach(element => {
          if (element.regex == 0) {
            if (element.cod_requirement == ConstantesConfig.COD_CONTRASENA_MINLEGTH) {
              arrayValidations.push(Validators.minLength(Number(element.value)));
            } else if (element.cod_requirement == ConstantesConfig.COD_CONTRASENA_MAXLEGTH) {
              arrayValidations.push(Validators.maxLength(Number(element.value)));
            }
          } else {
            arrayValidations.push(Validators.pattern(element.value));
          }
        });

        resolve(this.formBuilder.group({
          old_password: ['', Validators.required],
          password: ['', arrayValidations],
          password_confirmation: ['', Validators.required]
        },
          {
            validator: this.MustMatch('password', 'password_confirmation')
          }
        ))
      });
    });
  }

  /**
    * Método que permite la construcción del grupo de validaciones para los campos del formulario
    * para configuraciones de contraseñas
    * @returns objeto de tipo FormGroup con las validaciones de los campos
    */
  editConfigContrasenaFormValidators(): FormGroup {
    return this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      value: ['', Validators.required],
      message: ['', Validators.required],
      cod_requirement: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern(ConstantesConfig.REGEX_COD_REQUERIMIENTO)]]
    });
  }

  /**
    * Método que permite la construcción del grupo de validaciones para los campos del formulario
    * para estatus de usuarios
    * @returns objeto de tipo FormGroup con las validaciones de los campos
    */
  userEstatusFormValidators(): FormGroup {
    return this.formBuilder.group({
      cod_status: ['', [Validators.required, Validators.pattern(ConstantesConfig.REGEX_COD_STATUS)]],
      descripcion: ['', Validators.required],
      usa_fechas: ['0', [Validators.required, Validators.minLength(1), Validators.maxLength(1), Validators.pattern(ConstantesConfig.REGEX_BOOLEAN)]],
      valor_status: ['1', [Validators.required, Validators.minLength(1), Validators.maxLength(1), Validators.pattern(ConstantesConfig.REGEX_BOOLEAN)]]
    });
  }

  /**
    * Método que permite la construcción del grupo de validaciones para los campos del formulario
    * para asociar el estatus a un usuario
    * @returns objeto de tipo FormGroup con las validaciones de los campos
    */
  associateUserEstatusFormValidators(): FormGroup {
    return this.formBuilder.group({
      estatus: [null, Validators.required],
      fe_status_desde: [null],
      fe_status_hasta: [null]
    },
      {
        validator: this.DependsOnTwoValues('estatus', 'fe_status_desde', 'fe_status_hasta', 'usa_fechas', '0')
      }
    );
  }

}
