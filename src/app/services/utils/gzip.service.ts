import { Injectable } from '@angular/core';
import * as pako from 'pako';

/**
  * Clase representativa del servicio para manejo de información en relación a la
  * gestión de elementos bajo compresión de tipo gzip 
  */
@Injectable({
  providedIn: 'root'
})
export class GzipService {

  /**
    * Método constructor de la clase
    */
  constructor() { }

  /**
    * Método que permite realizar el ajuste del mensaje para los tiempos de carga de la
    * plantilla visual, a partir del mensaje obtenido como parámetro
    */
  inflate(dataEnB64: string): string {
    var b64Data = dataEnB64;

    var strData = atob(b64Data);

    var charData = strData.split('').map(function (x) { return x.charCodeAt(0); });

    var binData = new Uint8Array(charData);

    var data = pako.inflate(binData);

    var strData = this.arrayBufferToString(data);

    return strData;
  }

  /**
    * Método de utilidad que permite la transformación de un elemento arrayBuffer a string,
    * a partir del primero obtenido como parámetro
    */
  private arrayBufferToString(buffer) {

    var bufView = new Uint16Array(buffer);
    var length = bufView.length;
    var result = '';
    var addition = Math.pow(2, 16) - 1;

    for (var i = 0; i < length; i += addition) {

      if (i + addition > length) {
        addition = length - i;
      }
      result += String.fromCharCode.apply(null, bufView.subarray(i, i + addition));
    }

    return result;

  }

}
