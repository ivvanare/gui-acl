import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AppConfigService } from '../utils/config/app-config.service';
import { Contrasena } from '../models';
import { Router } from '@angular/router';

/**
  * Clase representativa del servicio para configuraciones de contraseña, como contenedora de los métodos
  * relacionados a la gestión de operaciones en base a las configuraciones de caracteres y funcionamiento
  * de las contraseñas para los usuarios
  */
@Injectable({
  providedIn: 'root'
})
export class ContrasenaService {

  /**
    * Método constructor de la clase
    */
  constructor(
    private http: HttpClient,
    private appConfigService: AppConfigService,
    private router: Router
  ) { }

  /**
    * Método que permite realizar la búsqueda de la dirección URL asociada a la gestión
    * de contraseñas, con el uso del servicio para configuración de la aplicación
    * @returns objeto de tipo String con la dirección URL, nulo en caso contrario
    */
  private getEndPoint(): string {
    return this.appConfigService.getConfig("URL_AUTH_API") + '/api/security/password';
  }

  /**
    * Método que permite obtener la lista de configuraciones para contraseñas disponibles en la aplicación,
    * con el uso de una petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  getList(): Observable<Contrasena[]> {
    return this.http.get<Contrasena[]>(this.getEndPoint());
  }

  /**
    * Método que permite realizar la búsqueda de información relacionada a las configuraciones de
    * contraseñas, a partir del número de página, columna y ordenamiento obtenidos como parámetros,
    * con el uso de una petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  getListPaginated(pagina: number, col: string, sort: string): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/page/${pagina}/${col}/${sort}`).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la activación de una configuración de contraseña para la aplicación,
    * a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  activate(idConfigContrasena: number): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/activate/${idConfigContrasena}`).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la desactivación de una configuración de contraseña para la aplicación,
    * a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  deactivate(idConfigContrasena: number): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/desactivate/${idConfigContrasena}`).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite realizar la búsqueda de información relacionada a configuración de contraseña,
    * a partir de su identificador obtenido como parámetro, con el uso de una petición web
    * a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  getById(idContrasena: Contrasena): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/edit/${idContrasena}`).pipe(
      catchError(e => {
        this.router.navigate(['/seguridad/usuarios']);

        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la actualización de una configuración de contraseña,
    * a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  update(contrasena: Contrasena): Observable<any> {
    return this.http.put<any>(`${this.getEndPoint()}/update/${contrasena.id}`, contrasena).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la creación de una configuración de contraseña,
    * a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  create(contrasena: Contrasena): Observable<any> {
    return this.http.post<any>(`${this.getEndPoint()}/create`, contrasena).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la eliminación de una configuración de contraseña,
    * a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  delete(idContrasena: number): Observable<any> {
    return this.http.delete<any>(`${this.getEndPoint()}/delete/${idContrasena}`).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

}
