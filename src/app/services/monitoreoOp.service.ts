import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfigService } from '../utils/config/app-config.service';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MonitoreoOpService {

  constructor(
    private httpClient: HttpClient,
    private appConfigService: AppConfigService

  ) {}

  private getEndPoint(): string {
    return this.appConfigService.getConfig("URL_BASE_API") + '/api';
  }

  // get(estatusOperacion){
  get(){
    // return this.httpClient.get(this.API_ENDPOINT_API+'operacion');
    // estatusOperacion
    // return this.httpClient.get<any>(`${this.getEndPoint()}/operacion?st_estatus_operacion=${estatusOperacion}`).pipe(
    return this.httpClient.get<any>(`${this.getEndPoint()}/operacion`).pipe(
      catchError(e => {
        return throwError(e);
      })
    );

  }

}
