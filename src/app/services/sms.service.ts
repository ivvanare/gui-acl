import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfigService } from '../utils/config/app-config.service';

@Injectable({
  providedIn: 'root'
})
export class SmsService {

  constructor(
    private httpClient: HttpClient,
    private appConfigService: AppConfigService
  ) {}

  private getEndPoint(): string {
    return this.appConfigService.getConfig("URL_BASE_API") + '/api';
  }

  get(numeros,mensaje){
    return this.httpClient.get(this.getEndPoint()+'enviar?numeros='+numeros+'&'+'mensaje='+mensaje+'&cliente=admin'+'&clave=secret');
  }

}
