import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Usuario } from '../models/usuario';
import { Router } from '@angular/router';
import { AppConfigService } from '../utils/config/app-config.service';

/**
  * Clase representativa del servicio de usuarios, como contenedora de los métodos relacionados
  * a la gestión de operaciones en base a los usuarios de la aplicación
  */
@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  /**
    * Método constructor de la clase
    */
  constructor(
    private http: HttpClient,
    private router: Router,
    private appConfigService: AppConfigService
  ) { }

  /**
    * Método que permite realizar la búsqueda de la dirección URL asociada a la seguridad de
    * usuarios, con el uso del servicio para configuración de la aplicación
    * @returns objeto de tipo String con la dirección URL, nulo en caso contrario
    */
  private getEndPoint(): string {
    return this.appConfigService.getConfig("URL_AUTH_API") + '/api/security/users';
  }

  /**
    * Método que permite obtener la lista de usuarios disponibles en la aplicación,
    * con el uso de una petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  getList(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.getEndPoint());
  }

  /**
    * Método que permite realizar la búsqueda de información relacionada a los usuarios de la
    * aplicación, a partir del número de página, columna y ordenamiento obtenidos como parámetros,
    * con el uso de una petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  getListPaginated(pagina: number, col: string, sort: string): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/page/${pagina}/${col}/${sort}`).pipe(
      map((response: any) => {
        (response.content as Usuario[])
        return response;
      })
    );
  }

  /**
    * Método que permite ejecutar la creación de un usuario para la aplicación,
    * a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  create(usuario: Usuario): Observable<any> {
    return this.http.post<any>(`${this.getEndPoint()}/create`, usuario).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la eliminación de un usuario para la aplicación,
    * a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  delete(idUsuario: number): Observable<any> {
    return this.http.delete<any>(`${this.getEndPoint()}/delete/${idUsuario}`).pipe(
      catchError(e => {
        console.error(e.error.mensaje);
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite realizar la búsqueda de información relacionada a un usuario de la
    * aplicación, a partir de su identificador obtenido como parámetro, con el uso de una petición web
    * a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  getById(idUsuario: number): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/edit/${idUsuario}`).pipe(
      catchError(e => {
        this.router.navigate(['/seguridad/usuarios']);

        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la actualización de un usuario para la aplicación,
    * a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  update(usuario: Usuario): Observable<any> {
    return this.http.put<any>(`${this.getEndPoint()}/update/${usuario.id}`, usuario).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la activación de un usuario para la aplicación,
    * a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  activate(idUsuario: number, estatus: any): Observable<any> {
    return this.http.put<any>(`${this.getEndPoint()}/activate/${idUsuario}`, estatus).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la desactivación de un usuario para la aplicación,
    * a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  deactivate(idUsuario: number, estatus: any): Observable<any> {
    return this.http.put<any>(`${this.getEndPoint()}/desactivate/${idUsuario}`, estatus).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite la búsqueda de un avatar asociado a un usuario, a partir del nombre
    * del mismo obtenido como parámetro
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  descargarAvatar(nombreAvatar: string): Observable<any> {
    return this.http.get(`${this.getEndPoint()}/avatar/${nombreAvatar}`, { responseType: 'blob' }).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite la carga de un avatar asociado a un usuario, a partir del objeto contenedor
    * del mismo obtenido como parámetro
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  cargarAvatar(archivo: File): Observable<HttpEvent<{}>> {
    let formData = new FormData();
    formData.append("avatar", archivo);

    const req = new HttpRequest('POST', `${this.getEndPoint()}/avatar`, formData, { reportProgress: true });

    return this.http.request(req).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  getHoraDB(){
    return this.http.get(`${this.appConfigService.getConfig("URL_AUTH_API")}/api/systemDateDB`).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }
}
