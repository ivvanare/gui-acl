import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppConfigService } from '../utils/config/app-config.service';
import { Permiso } from '../models';

/**
  * Clase representativa del servicio de permisos, como contenedora de los métodos relacionados
  * a la gestión de operaciones en base a los permisos asociados a los roles de usuarios
  */
@Injectable({
  providedIn: 'root'
})
export class PermisoService {

  /**
    * Método constructor de la clase
    */
  constructor(
    private http: HttpClient,
    private appConfigService: AppConfigService
  ) { }

  /**
    * Método que permite realizar la búsqueda de la dirección URL asociada a la seguridad de
    * usuarios, con el uso del servicio para configuración de la aplicación
    * @returns objeto de tipo String con la dirección URL, nulo en caso contrario
    */
  private getEndPoint(): string {
    return this.appConfigService.getConfig("URL_AUTH_API") + '/api/security/permissions';
  }

  /**
    * Método que permite realizar la búsqueda de la lista de permisos disponibles en
    * la aplicación
    * @returns objeto de tipo Observable con el resultado de la petición
    */
  getList(): Observable<Permiso[]> {
    return this.http.get<Permiso[]>(this.getEndPoint());
  }

}
