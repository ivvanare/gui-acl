import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError, of } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { Usuario } from '../../models/usuario';
import { AppConfigService } from '../../utils/config/app-config.service';
import { Rol } from '../../models/rol';
import { CookieService } from 'ngx-cookie-service';
import { CryptoService } from './crypto.service';
import { ConstantesConfig } from '../../utils/constant/config';

@Injectable({
  providedIn: 'root'
})

/**
* Clase representativa del servicio de autenticación, como contenedora de los métodos relacionados
* a la gestión de operaciones en base a usuarios y roles
*/
export class AuthService {

  private params: URLSearchParams;

  private _usuario: Usuario;

  private _token: string;

  private _refreshToken: string;

  private currentUserSubject: BehaviorSubject<Usuario>;

  public currentUser: Observable<Usuario>;

  private httpHeaders: HttpHeaders = new HttpHeaders({ 'Content-type': 'application/x-www-form-urlencoded' });
  

  /**
  * Método constructor de la clase
  */
  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    private cryptoService: CryptoService,
    private appConfigService: AppConfigService,
  ) {
    this.params = new URLSearchParams();
    this.currentUserSubject = new BehaviorSubject<Usuario>(this.usuario);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  /**
  * Método que permite realizar la búsqueda de la dirección URL asociada a la seguridad de
  * usuarios, con el uso del servicio para configuración de la aplicación
  * @returns objeto de tipo String con la dirección URL, nulo en caso contrario
  */
  private getEndPoint(): string {
    return this.appConfigService.getConfig("URL_AUTH_API") + '/api/auth';
  }

  /**
  * Método que permite realizar la búsqueda del encabezado para tipo de contenido a
  * transmitir en las peticiones del servicio
  * @returns objeto de tipo HttpHeaders con el encabezado, nulo en caso contrario
  */
  private getHeaders(): HttpHeaders {
    return this.httpHeaders;
  }

  /**
  * Método que permite realizar la búsqueda de la información relacionada al usuario desde
  * las ubicaciones ofrecidas por la aplicación: 1. Desde la memoria; 2. Desde las cookies
  * personalizadas en el navegador; 3. Nuevo usuario. La búsqueda se realiza de forma jerárquica,
  * obteniendose en el caso 2 desde los datos del token de acceso. Se utilizan los servicios
  * para manejo de cookies y de encriptación, así como el de configuración de la aplicación
  * @returns objeto de tipo Usuario con la información del usuario, objeto nuevo en caso contrario
  */
  public get usuario(): Usuario {
    if (this._usuario != null && this._usuario != undefined) {
      return this._usuario;
    } else if ((this._usuario == null || this._usuario == undefined) && this.cookieService.check(ConstantesConfig.COOKIE_TOKEN_NAME)) {
      let cookie = this.cookieService.get(ConstantesConfig.COOKIE_TOKEN_NAME);
      let tokenFromCookie = this.cryptoService.get(this.cryptoService.key, cookie);

      let payload = this.obtenerPayload(tokenFromCookie, 1);

      this._usuario = new Usuario();
      this._usuario.id = payload.user.id;
      this._usuario.name = payload.user.name;
      this._usuario.email = payload.user.email;

      payload.roles.forEach((rol: Rol) => {
        this._usuario.roles.push(rol);
      });

      this._usuario.avatar = payload.user.avatar;

      return this._usuario;
    }
    return new Usuario();
  }

  /**
  * Método que permite realizar la búsqueda de la información relacionada al token de acceso desde
  * las ubicaciones ofrecidas por la aplicación: 1. Desde la memoria; 2. Desde las cookies
  * personalizadas en el navegador. La búsqueda se realiza de forma jerárquica. Se utilizan los servicios
  * para manejo de cookies y de encriptación, así como el de configuración de la aplicación
  * @returns objeto de tipo String con el contenido del token, nulo en caso contrario
  */
  public get token(): string {
    if (this._token != null && this._token != undefined) {
      return this._token;
    } else if ((this._token == null || this._token == undefined) && this.cookieService.check(ConstantesConfig.COOKIE_TOKEN_NAME)) {
      let cookie = this.cookieService.get(ConstantesConfig.COOKIE_TOKEN_NAME);
      this._token = this.cryptoService.get(this.cryptoService.key, cookie);

      return this._token;
    }
    return null;
  }

  /**
  * Método que permite realizar la búsqueda de la información relacionada al token de refrescamiento de sesión desde
  * las ubicaciones ofrecidas por la aplicación: 1. Desde la memoria; 2. Desde las cookies
  * personalizadas en el navegador. La búsqueda se realiza de forma jerárquica. Se utilizan los servicios
  * para manejo de cookies y de encriptación, así como el de configuración de la aplicación
  * @returns objeto de tipo String con el contenido del token, nulo en caso contrario
  */
  public get refreshToken(): string {
    if (this._refreshToken != null && this._refreshToken != undefined) {
      return this._refreshToken;
    } else if ((this._refreshToken == null || this._refreshToken == undefined) && this.cookieService.check(ConstantesConfig.COOKIE_REFRESH_TOKEN_NAME)) {
      let cookie = this.cookieService.get(ConstantesConfig.COOKIE_REFRESH_TOKEN_NAME);
      this._refreshToken = this.cryptoService.get(this.cryptoService.key, cookie);

      return this._refreshToken;
    }
    return null;
  }

  /**
  * Método que permite realizar la búsqueda de la información relacionada al usuario,
  * con sesión activa, y desde la memoria de la aplicación
  * @returns objeto de tipo Usuario si existe en memoria, null en caso contrario
  */
  public get currentUserValue(): Usuario {
    return this.currentUserSubject.value;
  }

  /**
  * Método que permite ejecutar el proceso de acceso a la aplicación, a partir de los datos del
  * usuario en conjunto con los parametros de la aplicación, a través de una
  * petición web a la dirección URL asociada a la seguridad de usuarios. Se utiliza el servicio de
  * configuración de la aplicación
  * @returns objeto de tipo Observable con los datos de la sesión del usuario, excepción en caso contrario
  */
  login(usuario: Usuario): Observable<any> {
    this.params.set('email', usuario.email);
    this.params.set('password', usuario.password);
    this.params.set('grant_type', this.appConfigService.getConfig('DATA_AUTH_GRANT_APP'));
    this.params.set('client_id', this.appConfigService.getConfig('DATA_AUTH_CLIENTE'));

    return this.http.post<any>(`${this.getEndPoint()}/login`, this.params.toString(), { headers: this.getHeaders() }).pipe(
      catchError(e => {
        return throwError(e);
      }),
      tap(response => {
        this.guardarPayload(response);
        this.guardarTokens(response);
      })
    );
  }

  
  /**
  * Método que permite ejecutar el proceso de salida de la aplicación, a partir de los datos del
  * usuario, a través de una petición web a la dirección URL asociada a la seguridad de usuarios
  * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
  */
  logout(): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/logout`).pipe(
      tap(response => {
        if (response.success) {
          this.deleteUserInfo();
        }
      }),
      catchError(e => {
        if (e.status == 401) {
          this.deleteUserInfo();
        }
        return throwError(e);
      })
    );
  }

  /**
  * Método que permite ejecutar el proceso de refrescamiento de la sesión, a partir de los datos del
  * usuario en conjunto con los parametros de la aplicación, a través de una
  * petición web a la dirección URL asociada a la seguridad de usuarios. Se utiliza el servicio de
  * configuración de la aplicación
  * @returns objeto de tipo Observable con los datos de la sesión del usuario, excepción en caso contrario
  */
  refreshSession(): Observable<any> {
    this.params.set("grant_type", ConstantesConfig.DATA_AUTH_GRANT_REFRESH);
    this.params.set("refresh_token", this.refreshToken);
    this.params.set("client_id", this.appConfigService.getConfig("DATA_AUTH_CLIENTE"));

    return this.http.post<any>(`${this.getEndPoint()}/refresh-session`, this.params.toString(), { headers: this.getHeaders() }).pipe(
      catchError(e => {
        if (e.status == 500) {
          this.deleteUserInfo();
        }
        return throwError(e);
      }),
      tap(response => {
        this.guardarPayload(response);
        this.guardarTokens(response);
      })
    );
  }

  /**
  * Método que permite guardar la información del usuario que accede a la aplicación en la memoria,
  * a partir de un objeto de respuesta recibido como parámetro
  */
  private guardarPayload(response: any): void {
    let payload = this.obtenerPayload(response.access_token, 1);

    this._usuario = new Usuario();
    this._usuario.id = payload.user.id;
    this._usuario.name = payload.user.name;
    this._usuario.email = payload.user.email;
    this._usuario.avatar = payload.user.avatar,

    payload.roles.forEach((rol: Rol) => {
      this._usuario.roles.push(rol);
    });

    this._usuario.avatar = payload.user.avatar;

    this.currentUserSubject.next(this._usuario);
  }

  /**
  * Método que permite guardar la información de los tokens de acceso y refrescamiento de sesión
  * en la memoria, así como en cookies personalizadas, a partir de un objeto de respuesta
  * recibido como parámetro. Se utilizan los servicios de configuración de la aplicación, de
  * encriptación y de manejo de cookies
  */
  private guardarTokens(response: any): void {
    this._token = response.access_token;
    this._refreshToken = response.refresh_token;
    this.cookieService.set(
      ConstantesConfig.COOKIE_TOKEN_NAME,
      this.cryptoService.set(this.cryptoService.key, this._token),
      +response.expires_in / 86400,
      "/",
      null,
      ConstantesConfig.SSL_ESTATUS,
      "Strict"
    );
    this.cookieService.set(
      ConstantesConfig.COOKIE_REFRESH_TOKEN_NAME,
      this.cryptoService.set(this.cryptoService.key, this._refreshToken),
      (+response.expires_in + 300) / 86400,
      "/",
      null,
      ConstantesConfig.SSL_ESTATUS,
      "Strict"
    );
  }

  /**
  * Método que permite obtener la información específica de una de las secciones pertenecientes
  * al token de acceso
  * @returns objeto de tipo Any que contiene los datos desde el token, parseados a JSON, nulo en
  * caso contrario
  */
  obtenerPayload(accessToken: string, pos: number): any {
    if (accessToken != null) {
      return JSON.parse(atob(accessToken.split(".")[pos]));
    }
    return null;
  }

  /**
  * Método que permite verificar si se encuentra autenticado un usuario en la sesión actual,
  * con el uso de los servicios para manejo de cookies y de configuración de la aplicación
  * @returns true si se encuentra un usuario autenticado válido, false en caso contrario
  */
  isAuthenticated(): boolean {
    let payload = this.obtenerPayload(this._token, 1);
    if (payload != null && this._usuario != null && payload.sub == this._usuario.id && payload.aud == this.appConfigService.getConfig("DATA_AUTH_CLIENTE")) {
      return true;
    }
    return false;
  }

  /**
  * Método que permite eliminar los datos asociados a un usuario de la aplicación
  */
  private deleteUserInfo(): void {
    this._token = null;
    this._usuario = null;
    this._refreshToken = null;
    this.cookieService.delete(ConstantesConfig.COOKIE_TOKEN_NAME);
    this.cookieService.delete(ConstantesConfig.COOKIE_REFRESH_TOKEN_NAME);
    this.currentUserSubject.next(null);
  }

  /**
  * Método que permite verificar si una lista de permisos en formato textual contiene algún
  * permiso válido en la sesión actual
  * @returns true si es válido, false en caso contrario
  */
  public hasPermissions(listaPermisos: Array<string>): boolean {
    let isEnabled: boolean = false;

    listaPermisos.forEach(permiso => {
      if (!isEnabled) {
        isEnabled = this.hasPermissionsAsigned(permiso);
      }
    });

    return isEnabled;
  }

  /**
  * Método que permite verificar si un permiso obtenido desde un parámetro se encuentra
  * asociado al usuario con sesión activa en la aplicación
  * @returns true si se encuentra asociado, false en caso contrario
  */
  public hasPermissionsAsigned(perm: string) {
    if (this.isAuthenticated() && (this.usuario.getAsignedPermissions().includes(perm) || this.usuario.hasAllPermissions())) {
      return true;
    }
    return false;
  }

  /**
  * Método que permite ejecutar la validación de credenciales, a partir de los datos del
  * usuario en conjunto con los parametros de la aplicación, a través de una
  * petición web a la dirección URL asociada a la seguridad de usuarios. Se utiliza el servicio de
  * configuración de la aplicación
  * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
  */
  checkCredenciales(email: string, password: string): Observable<any> {
    this.params.set("email", email);
    this.params.set("password", password);
    this.params.set("grant_type", this.appConfigService.getConfig("DATA_AUTH_GRANT_APP"));
    this.params.set("client_id", this.appConfigService.getConfig("DATA_AUTH_CLIENTE"));

    return this.http.post<any>(`${this.getEndPoint()}/user/credenciales/check`, this.params.toString(), { headers: this.getHeaders() }).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }
}
