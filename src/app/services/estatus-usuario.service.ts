import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AppConfigService } from 'src/app/utils/config/app-config.service';
import { Router } from '@angular/router';
import { EstatusUsuario } from '../models';

/**
  * Clase representativa del servicio para estatus de los usuarios, como contenedora de los métodos
  * relacionados a la gestión de operaciones en base a los estatus presentados por los usuarios con
  * base en la aplicación
  */
@Injectable({
  providedIn: 'root'
})
export class EstatusUsuarioService {

  /**
    * Método constructor de la clase
    */
  constructor(
    private http: HttpClient,
    private appConfigService: AppConfigService,
    private router: Router
  ) { }

  /**
    * Método que permite realizar la búsqueda de la dirección URL asociada a los estatus de
    * los usuarios, con el uso del servicio para configuración de la aplicación
    * @returns objeto de tipo String con la dirección URL, nulo en caso contrario
    */
  private getEndPoint(): string {
    return this.appConfigService.getConfig("URL_AUTH_API") + '/api/security/estatus-user';
  }

  /**
    * Método que permite obtener la lista de los estatus para usuarios disponibles en la aplicación,
    * con el uso de una petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  getList(): Observable<EstatusUsuario[]> {
    return this.http.get<EstatusUsuario[]>(this.getEndPoint());
  }

  /**
    * Método que permite realizar la búsqueda de información relacionada a los estatus de
    * los usuarios, a partir del número de página, columna y ordenamiento obtenidos como parámetros,
    * con el uso de una petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  getListPaginated(pagina: number, col: string, sort: string): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/page/${pagina}/${col}/${sort}`).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite realizar la búsqueda de información relacionada a un estatus de usuario,
    * a partir de su identificador obtenido como parámetro, con el uso de una petición web
    * a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  getById(idEstatus: EstatusUsuario): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/edit/${idEstatus}`).pipe(
      catchError(e => {
        this.router.navigate(['/seguridad/estatus-usuarios']);

        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la actualización de un estatus de usuario,
    * a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  update(estatus: EstatusUsuario): Observable<any> {
    return this.http.put<any>(`${this.getEndPoint()}/update/${estatus.id}`, estatus).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la creación de un estatus de usuario,
    * a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  create(estatus: EstatusUsuario): Observable<any> {
    return this.http.post<any>(`${this.getEndPoint()}/create`, estatus).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la eliminación de un estatus de usuario,
    * a través de sus datos obtenidos como parámetros, con el uso de una
    * petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  delete(idEstatus: number): Observable<any> {
    return this.http.delete<any>(`${this.getEndPoint()}/delete/${idEstatus}`).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

}
