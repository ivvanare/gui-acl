import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Parametro } from '../models/parametro';
import { Router } from '@angular/router';
import { AuthService } from './auth/auth.service';
import { AppConfigService } from 'src/app/utils/config/app-config.service';
import { FechaService } from './utils/fecha.service';

/**
  * Clase representativa del servicio de parámetros, como contenedora de los métodos relacionados
  * a la gestión de operaciones en base a los parámetros de la aplicación
  */
@Injectable({
  providedIn: 'root'
})
export class ParametroService {

  private currentDateSubject: BehaviorSubject<any>;

  public currentDate: Observable<any>;

  private currentStatusSistemaSubject: BehaviorSubject<any>;

  public currentStatusSistema: Observable<any>;

  /**
    * Método constructor de la clase
    */
  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: AuthService,
    private appConfigService: AppConfigService,
    private fechaService: FechaService
  ) {
    this.currentDateSubject = new BehaviorSubject<any>(this.fechaService.getDateInObject(this.getFechaBd()));
    this.currentDate = this.currentDateSubject.asObservable();
    this.currentStatusSistemaSubject = new BehaviorSubject<any>("0");
    this.currentStatusSistema = this.currentStatusSistemaSubject.asObservable();
  }

  /**
    * Método que permite realizar la búsqueda de la dirección URL asociada a la información de
    * parámetros, con el uso del servicio para configuración de la aplicación
    * @returns objeto de tipo String con la dirección URL, nulo en caso contrario
    */
  private getEndPoint(): string {
    return this.appConfigService.getConfig("URL_BASE_API") + '/api/configuracion/parametros';
  }

  /**
    * Método que permite realizar la búsqueda de la información relacionada a la fecha actual
    * del sistema, y desde la memoria de la aplicación
    * @returns objeto de tipo Any si existe en memoria, null en caso contrario
    */
  public get currentDateValue(): any {
    return this.currentDateSubject.value;
  }

  /**
    * Método que permite realizar la búsqueda de la información relacionada al estado actual
    * del sistema, y desde la memoria de la aplicación
    * @returns objeto de tipo Any si existe en memoria, null en caso contrario
    */
  public get currentStatusSistemaValue(): any {
    return this.currentStatusSistemaSubject.value;
  }

  /**
    * Método que permite realizar la búsqueda de información relacionada a los parámetros
    * de la aplicación, a partir del número de página, columna y ordenamiento obtenidos como parámetros,
    * con el uso de una petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  getListPaginated(pagina: number, col: string, sort: string): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/page/${pagina}/${col}/${sort}`).pipe(
      map((response: any) => {
        (response.content as Parametro[])
        return response;
      })
    );
  }

  /**
    * Método que permite ejecutar la creación de un parámetro para la aplicación, a través de
    * sus datos obtenidos como parámetros, con el uso de una petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  create(parametro: Parametro): Observable<any> {
    return this.http.post<any>(this.getEndPoint(), parametro).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la eliminación de un parámetro para la aplicación, a través de
    * sus datos obtenidos como parámetros, con el uso de una petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  delete(idParametro: string): Observable<any> {
    return this.http.delete<any>(`${this.getEndPoint()}/${idParametro}`).pipe(
      catchError(e => {
        console.error(e.error.mensaje);
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite realizar la búsqueda de información relacionada a un parámetro, a partir
    * de su identificador obtenido como parámetro, con el uso de una petición web
    * a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  getById(idParametro: string): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/${idParametro}`).pipe(
      catchError(e => {
        this.router.navigate(['/configuracion/parametros']);
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite ejecutar la actualización de un parámetro para la aplicación, a través de
    * sus datos obtenidos como parámetros, con el uso de una petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  update(parametro: Parametro): Observable<any> {
    return this.http.put<any>(this.getEndPoint(), parametro).pipe(
      tap((param: any) => {
        if (param.parametro.id == "005") {
          this.currentStatusSistemaSubject.next(param.parametro as Parametro);
        }
      }),
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite actualizar la información del estado de la aplicación en la memoria, con
    * respecto a las fechas de transmisiín de archivos
    */
  updateInAppFromBd(parametro: Parametro) {
    if (parametro.id == "005") {
      this.currentStatusSistemaSubject.next(parametro);
    }
  }

  /**
    * Método que permite realizar la búsqueda de información relacionada a la fecha del sistema desde
    * base de datos, con el uso de una petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  getFechaBd(): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/fecha-basededatos`);
  }

  /**
    * Método que permite realizar el cálculo de la fecha del sistema y presentarla en formato HTML5
    * @returns objeto de tipo String con la fecha actualizada en formato específico
    */
  calcularFechaSistema(fechaSistema: string): string {
    let fechaNew = this.fechaService.getDateInObjectFromFormat(fechaSistema, this.fechaService.DATE_FORMAT_HTML5_MOM).add(1, 'seconds');
    this.currentDateSubject.next(fechaNew);
    return fechaNew.format(this.fechaService.DATE_FORMAT_HTML5_MOM);
  }

  /**
    * Método que permite realizar la búsqueda de información relacionada al estado del sistema desde
    * base de datos, con el uso de una petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  getEstadoSistema(): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/estado-sistema`).pipe(
      tap((param: any) => {
        this.currentStatusSistemaSubject.next(param.estadoSistema as Parametro);
      }),
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite realizar la búsqueda de información relacionada a la barra de navegación,
    * con el uso de una petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  private getDatosBarraNavegacion(): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/datos-barranavegacion`).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite realizar la búsqueda de información relacionada a la barra de navegación,
    * y realizar los ajustes de configuración
    * @returns objeto de tipo Parametro con los datos de la barra de navegación, nuevo objeto predeterminado
    * en caso contrario
    */
  getColorBarraNavegacion(): Parametro {
    let colorBarraNavegacion: Parametro;

    if (this.authService.isAuthenticated()) {
      this.getDatosBarraNavegacion().subscribe(response => {
        colorBarraNavegacion = response.datosBarra as Parametro;
      });
    }

    if (!colorBarraNavegacion) {
      colorBarraNavegacion = new Parametro();
      colorBarraNavegacion.id = "008";
      colorBarraNavegacion.descripcion = "COLOR PARA BARRA DE NAVEGACION (HEXADECIMAL)";
      colorBarraNavegacion.valor = '#FFFFFF';
    }

    return colorBarraNavegacion;
  }

  /**
    * Método que permite realizar la búsqueda de información relacionada a la información básica
    * de la aplicación, con el uso de una petición web a la dirección URL asociada
    * @returns objeto de tipo Observable con el resultado de la petición, excepción en caso contrario
    */
  getInfoAplicacion(): Observable<any> {
    return this.http.get<any>(`${this.getEndPoint()}/info-aplicacion`).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  /**
    * Método que permite realizar la verificación del estado de la aplicación a partir de la información
    * proporcionada por el parámetro asignado
    * @returns true si el estado es 1, false en caso contrario
    */
  checkEstatusTransmisiones(parametro: Parametro): boolean {
    if (parametro.valor == '1') {
      return true;
    }
    return false;
  }  

}
