import { Component, OnDestroy } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AppConfigService } from './utils/config/app-config.service';
import { AuthService } from './services/auth/auth.service';

/**
  * Representa la clase contenedora de los métodos a ejecutar en las operaciones relacionadas a la
  * carga inicial de la aplicación
  */
 @Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnDestroy {

  autenticado: boolean;

  private obsParam$: Subject<void> = new Subject<void>();

  /**
    * Método constructor de la clase.
    * Permite validar el estado de autenticación del usuario en la instancia actual de
    * la aplicación
    */
  constructor(
    private authService: AuthService,
    public appConfigService: AppConfigService
  ) {
    this.authService.currentUser.pipe(
      takeUntil(this.obsParam$)
    ).subscribe(usuario => {
      this.autenticado = usuario != null && usuario != undefined && usuario.roles.length > 0 ? true : false;
    });
  }

  /**
    * Método que se ejecuta al momento de destrucción de la clase.
    */
  ngOnDestroy() {
    this.obsParam$.next();
    this.obsParam$.complete();
  }

}
