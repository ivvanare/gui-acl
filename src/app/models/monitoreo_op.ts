export class MonitoreoOp {

  id_operacion: string;
  id_operacion_emisor: string;
  id_operacion_ncce: string;
  id_msgid_mensaje: string;
  co_participante_emisor: string;
  co_participante_receptor: string;
  co_moneda: string;
  mo_monto: string;
  st_estatus_operacion: string;
  co_resultado: string;
  fecha_timestamp_ins:string;
  fecha_timestamp_upd:string;
  co_tipo_operacionstring: string;
  tiempo_promedio:string;
}
