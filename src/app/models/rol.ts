import { Permiso } from './permiso';

export class Rol {

  id: number;
  name: string;
  status: number;
  all_permissions: string = "1";
  sort: number;
  createdAt: Date;
  updatedAt: Date;
  permissions: Array<Permiso> = [];

  public getAsignedPermissions(): Array<string>{
    let lista: Array<string> = [];
    this.permissions.forEach( perm => {
      lista.push(perm.name);
    })
    return lista;
  }

}
