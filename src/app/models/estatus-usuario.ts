export class EstatusUsuario {

  id: number;
  cod_status: number;
  descripcion: string;
  usa_fechas: number;
  valor_status: number;

}
