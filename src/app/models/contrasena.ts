export class Contrasena {

  id: number;
  name: string;
  status: number;
  description: string;
  value: string;
  cod_requirement: string;
  regex: number;
  message: string;

}
