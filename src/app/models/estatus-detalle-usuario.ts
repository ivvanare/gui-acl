export class EstatusDetalleUsuario {

  id: number;
  created_at: Date;
  fe_status_desde: Date;
  fe_status_hasta: Date;
  cod_status: number;
  updated_at: Date;
  user_id: number

}
