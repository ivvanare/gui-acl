import { Rol } from './rol';
import { EstatusDetalleUsuario } from './estatus-detalle-usuario';

export class Usuario {

  id: number;
  name: string;
  password: string;
  password_confirmation: string;
  email: string;
  email_verified_at: Date;
  activation_token: string;
  remember_token: string;
  active: number;
  roles: Array<Rol> = [];
  deleted_at: Date;
  created_at: Date;
  updated_at: Date;
  avatar: string;
  descripcion_estatus: EstatusDetalleUsuario;

  public constructor(init?: Partial<Usuario>) {
    Object.assign(this, init);
  }

  public getAsignedRoles(): Array<string> {
    let lista: Array<string> = [];

    this.roles.forEach(rol => {
      lista.push(rol.name);
    });

    return lista;
  }

  public getAsignedPermissions(): Array<string> {
    let lista: Array<string> = [];

    this.roles.forEach(rol => {
      rol.permissions.forEach(permiso => {
        lista.push(permiso.name);
      })
    });

    return lista;
  }

  public hasAllPermissions(): boolean {
    let isAdmin: boolean = false;

    this.roles.forEach(rol => {
      if (rol.all_permissions == "1" && !isAdmin) {
        isAdmin = true;
      }
    });

    return isAdmin;
  }

  public getAsignedPermissionsDisplayName(): Array<string> {
    let lista: Array<string> = [];

    this.roles.forEach(rol => {
      rol.permissions.forEach(permiso => {
        lista.push(permiso.display_name);
      })
    });

    return lista;
  }

}
