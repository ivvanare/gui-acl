export interface CorreoMensajes {
  destinatario: string[];
  asunto:string;
  contenido:string;
  cliente:string;
  clave:string;

}
