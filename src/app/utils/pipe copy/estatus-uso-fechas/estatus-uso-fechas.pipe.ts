import { Pipe, PipeTransform } from '@angular/core';

/**
  * Clase representativa de un evento ejecutable para transformación de datos,
  * permitiendo al usuario la habilidad de transformar un elemento a otro con mayor representación
  * del objetivo, relacionado al estatus de los usuarios en la aplicación y uso de rangos de fechas
  */
@Pipe({
  name: 'estatususofechas',
  pure: true
})
export class GetStatusUsoFechasPipe implements PipeTransform {

  /**
    * Método ejecutable que permite transformar un dato, a partir del valor obtenido como
    * parámetro
    * @returns objeto de tipo String con la nueva representación del dato
    */
  transform(value: number): string {
    return this.getStatusUser(value);
  }

  /**
    * Método que permite ejecutar la transformación de un dato, a partir del valor obtenido como
    * parámetro
    * @returns objeto de tipo String con la nueva representación del dato
    */
  getStatusUser(estatus: number): string {
    if (estatus == 0) {
      return 'No';
    }
    return 'Si';
  }
}
