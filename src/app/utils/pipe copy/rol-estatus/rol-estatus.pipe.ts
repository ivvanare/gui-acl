import { Pipe, PipeTransform } from '@angular/core';

/**
  * Clase representativa de un evento ejecutable para transformación de datos,
  * permitiendo al usuario la habilidad de transformar un elemento a otro con mayor representación
  * del objetivo, relacionado a los estados de los roles
  */
@Pipe({
  name: 'estatusrol',
  pure: true
})
export class GetStatusRolPipe implements PipeTransform {

  /**
    * Método ejecutable que permite transformar un dato, a partir del valor obtenido como
    * parámetro
    * @returns objeto de tipo SafeHtml con la nueva representación del dato
    */
  transform(value: number): string {
    return this.getStatusRol(value);
  }

  /**
    * Método que permite ejecutar la transformación de un dato, a partir del valor obtenido como
    * parámetro
    * @returns objeto de tipo String con la nueva representación del dato
    */
  getStatusRol(estatus: number): string {
    if (estatus == 0) {
      return 'INACTIVO';
    }
    return 'ACTIVO';
  }
}
