import { Pipe, PipeTransform } from '@angular/core';
import { ConstantesConfig } from '../../constant/config';

/**
  * Clase representativa de un evento ejecutable para transformación de datos,
  * permitiendo al usuario la habilidad de transformar un elemento a otro con mayor representación
  * del objetivo, relacionado al reconocimiento del mensaje relacionado a un patrón, dentro de las
  * configuraciones de contraseñas
  */
@Pipe({
  name: 'mensajepatroncontrasena',
  pure: true
})
export class GetMensajePatronContrasenaPipe implements PipeTransform {

  /**
    * Método ejecutable que permite transformar un dato, a partir del valor obtenido como
    * parámetro
    * @returns objeto de tipo String con la nueva representación del dato
    */
  transform(value: string, contrasenaValidators: any[]): string {
    return this.getMensajePatronContrasena(value, contrasenaValidators);
  }

  /**
    * Método que permite ejecutar la transformación de un dato, a partir del valor obtenido como
    * parámetro
    * @returns objeto de tipo String con la nueva representación del dato
    */
  getMensajePatronContrasena(patron: string, contrasenaValidators: any[]): string {
    var mensajes = contrasenaValidators.filter(c =>
      c.cod_requirement != ConstantesConfig.COD_CONTRASENA_MINLEGTH &&
      c.cod_requirement != ConstantesConfig.COD_CONTRASENA_MAXLEGTH &&
      [patron.substring(1, patron.length - 1)].includes(c.value)
    );

    return mensajes.length > 0 && mensajes[0] != null && mensajes[0] != undefined ? mensajes[0].message : 'Mensaje no identificado';
  }
}
