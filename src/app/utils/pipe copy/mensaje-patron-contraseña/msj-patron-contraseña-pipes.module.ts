import { NgModule } from '@angular/core';
import { GetMensajePatronContrasenaPipe } from './mensaje-patron-contrasena.pipe';

@NgModule({
    imports: [
    ],
    declarations: [
        GetMensajePatronContrasenaPipe
    ],
    exports: [
        GetMensajePatronContrasenaPipe
    ]
})
export class GetMensajePatronContrasenaPipeModule { 
    
}