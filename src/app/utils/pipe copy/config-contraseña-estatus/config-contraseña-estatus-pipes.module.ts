import { NgModule } from '@angular/core';
import { GetStatusConfigContrasenaPipe } from './config-contrasena-estatus.pipe';

@NgModule({
    imports: [
    ],
    declarations: [
        GetStatusConfigContrasenaPipe
    ],
    exports: [
        GetStatusConfigContrasenaPipe
    ]
})
export class ConfigContraseñaEstatusPipesModule { }