import { Pipe, PipeTransform } from '@angular/core';
import { EstatusUsuario, EstatusDetalleUsuario } from '../../../models';

/**
  * Clase representativa de un evento ejecutable para transformación de datos,
  * permitiendo al usuario la habilidad de transformar un elemento a otro con mayor representación
  * del objetivo, relacionado al reconocimiento del detalle para un estatus del usuario
  */
@Pipe({
  name: 'estatusdetalleuser',
  pure: true
})

export class GetStatusDetalleUserPipe implements PipeTransform {

  /**
    * Método ejecutable que permite transformar un dato, a partir del valor obtenido como
    * parámetro
    * @returns objeto de tipo String con la nueva representación del dato
    */
  transform(value: EstatusDetalleUsuario, detalles: EstatusUsuario[]): string {
    return this.getDetalleStatus(value, detalles);
  }

  /**
    * Método que permite ejecutar la transformación de un dato, a partir del valor obtenido como
    * parámetro
    * @returns objeto de tipo String con la nueva representación del dato
    */
  getDetalleStatus(valor: EstatusDetalleUsuario, detalles: EstatusUsuario[]): string {
    var mensaje = detalles.filter(c => Number(c.cod_status) == (valor == null ? null : valor.cod_status));

    return mensaje.length > 0 && mensaje[0] != null && mensaje[0] != undefined ? (mensaje[0].descripcion + (mensaje[0].usa_fechas == 1 ? ` (${valor.fe_status_desde} a ${valor.fe_status_hasta})` : '')) : 'No identificada';
  }
}
