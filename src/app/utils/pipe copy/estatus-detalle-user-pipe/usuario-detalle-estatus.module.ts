import { NgModule } from '@angular/core';
import { GetStatusDetalleUserPipe } from './usuario-detalle-estatus.pipe';

@NgModule({
    imports: [
    ],
    declarations: [
        GetStatusDetalleUserPipe
    ],
    exports: [
        GetStatusDetalleUserPipe
    ]
})

export class StatusDetalleUserPipeModule { }