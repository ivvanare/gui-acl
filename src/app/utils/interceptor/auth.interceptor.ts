import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { AlertaService } from 'src/app/services/utils/alerta.service';
import { AuthService } from 'src/app/services/auth/auth.service';

/**
  * Clase representativa de un evento ejecutable para verificaciones en las peticiones web,
  * a través de un manejador de eventos, permitiendo a la aplicación centralizar el manejo de errores
  */
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  /**
    * Método constructor de la clase
    */
  constructor(
    private authService: AuthService,
    private router: Router,
    private alertaService: AlertaService
  ) { }

  /**
    * Método ejecutable que permite verificar el estado de un error generado en una petición web,
    * con el uso del servicio de autenticación
    * @returns objeto de tipo Observable con el resultado de la petición
    */
  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {

    if (!this.authService.isAuthenticated()) {
      this.router.navigate(["/login"]);
      return next.handle(req);
    }

    return next.handle(req).pipe(
      catchError(e => {

        switch (e.status) {
          case 0:
            this.alertaService.doSwalError({ title: "Error", text: "Falla de conexión" });
            break;

          case 400:
            let error = "";
            if (e.message) { error = e.message } else { error = e.error.mensaje; }
            this.alertaService.doSwalError({ title: "Error", text: error });
            break;

          case 401: //Unauthorized
            if (e.error.message == "Unauthenticated.") {
              this.authService.logout().subscribe(res => {
                this.alertaService.doSwalError({ title: "Error de autorización", text: `Existe un error en las autorizaciones del usuario: ${e.error.message}. ${res.message}` });
                this.router.navigate(["/login"]);
              });
            } else {
              this.alertaService.doSwalToast({ type: 'error', title: e.error.message });
            }
            break;

          case 403: //Forbidden
            this.alertaService.doSwalWarning({ title: "Acceso denegado", text: `Usuario ${this.authService.usuario.name} no tiene permisos` });
            this.router.navigate(["/inicio"]);
            break;

          case 422: //Unprocessable Entity
            this.alertaService.doSwalError({ title: "Error de validación", text: 'Existen campos con inconsistencias' });
            break;

          case 429: //Too Many Requests
            this.alertaService.doSwalError({ title: "Error", text: 'Se han realizado demasiadas peticiones al servidor. Intente mas tarde' });
            break;

          case 500:
            this.alertaService.doSwalError({ title: e.name, text: e.error.error });
            break;

          default:
            this.alertaService.doSwalError({ title: "Error", text: "Falla de conexión" });
            break;
        }

        return throwError(e);
      })
    );
  }
}
