import { Injectable, OnDestroy } from '@angular/core';
import { Router, CanActivate, UrlTree } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { AuthService } from '../../services/auth/auth.service';
import { ParametroService } from '../../services/parametro.service';
import { takeUntil } from 'rxjs/operators';
import { AlertaService } from '../../services/utils/alerta.service';

/**
  * Clase representativa de un evento ejecutable para autenticación, a través de un manejador de eventos,
  * permitiendo al usuario la habilidad de acceder a un componente
  */
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, OnDestroy {

  private fecha: any;

  private obsParam$: Subject<void> = new Subject<void>();

  /**
    * Método constructor de la clase
    */
  constructor(
    private authService: AuthService,
    private router: Router,
    private parametroService: ParametroService,
    private alertaService: AlertaService,
  ) { }

  /**
    * Método ejecutable que permite verificar si se encuentra un usuario con sesión activa en la
    * aplicación, verificando la información relacionada al token de acceso, con el uso del
    * servicio de autenticación
    * @returns objeto de tipo Observable, Promise, boolean o UrlTree, con el resultado de las
    * verificaciones
    */
  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.authService.isAuthenticated()) {

      if (this.isTokenExpirado()) {
        this.authService.logout().subscribe(res => {
          this.alertaService.doSwalToast({ type: 'info', title: `La sesión ha expirado. ${res.message}` });
          this.router.navigate(['/login']);
        });

        return false;
      }

      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }

  /**
    * Método que permite verificar si la información relacionada a la fecha de expiración del
    * token es válida, comparandola con la fecha actual de la aplicación, con el uso del
    * servicio de parámetros
    * @returns true si el token de acceso expiró, false en caso contrario
    */
  private isTokenExpirado(): boolean {
    let token = this.authService.token;
    let payload = this.authService.obtenerPayload(token, 1);

    this.parametroService.currentDate.pipe(
      takeUntil(this.obsParam$)
    ).subscribe(fecha => {
      this.fecha = fecha;
    });

    let now = this.fecha;
    let diff = payload.exp - now.unix();

    if (diff <= 10) {
      return true;
    } else if (diff < 300) {
      this.alertaService.doSwalToast({ 
        type: 'info',
        title: `La sesión del usuario ${this.authService.usuario.name} expira en ${diff} segundos`
      });
    }
    return false;
  }

  /**
    * Método que se ejecuta al momento de destrucción de la clase.
    */
  ngOnDestroy() {
    this.obsParam$.next();
    this.obsParam$.complete();
  }

}
