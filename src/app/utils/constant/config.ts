export abstract class ConstantesConfig {

  static readonly COOKIE_MONITOR_REINTENTOS_NAME: string = 'mft_monitor_reintentos';
  static readonly COOKIE_TOKEN_NAME: string = 'eps-inm_token';
  static readonly COOKIE_REFRESH_TOKEN_NAME: string = 'eps-inm_refresh_token';

  static readonly DATA_AUTH_GRANT_REFRESH: string = 'refresh_token';

  static readonly REFRESH_SYSTEM_TIME: number = 1000;

  static readonly SSL_ESTATUS: boolean = false;

  static readonly REGEX_SEGUNDOS: any = '([0-9]{2,})|([1-9]{1,})';
  static readonly REGEX_ID_PARAMETRO: any = '([0-9]{3})';
  static readonly REGEX_COD_RUTA: any = '([0-9]{4})';
  static readonly REGEX_COD_REQUERIMIENTO: any = '([0-9]{4})';
  static readonly REGEX_BOOLEAN: any = '([0-1]{1})';
  static readonly REGEX_COD_STATUS: any = '([0-9])';

  static readonly COD_CONTRASENA_MINLEGTH: string = '0001';
  static readonly COD_CONTRASENA_MAXLEGTH: string = '0002';
}
