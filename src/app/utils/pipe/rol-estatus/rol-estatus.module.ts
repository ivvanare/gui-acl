import { NgModule } from '@angular/core';
import { GetStatusRolPipe } from './rol-estatus.pipe';

@NgModule({
    imports: [
    ],
    declarations: [
        GetStatusRolPipe
    ],
    exports: [
        GetStatusRolPipe
    ]
})

export class RolEstatusPipeModule { }