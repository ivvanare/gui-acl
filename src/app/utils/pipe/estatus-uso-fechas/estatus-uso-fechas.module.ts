import { NgModule } from '@angular/core';
import { GetStatusUsoFechasPipe } from './estatus-uso-fechas.pipe';

@NgModule({
    imports: [
    ],
    declarations: [
        GetStatusUsoFechasPipe
    ],
    exports: [
        GetStatusUsoFechasPipe
    ]
})

export class StatusUsoFechasPipeModule { }