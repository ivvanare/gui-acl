import { NgModule } from '@angular/core';
import { GetStatusUserPipe } from './usuario-estatus.pipe';

@NgModule({
    imports: [
    ],
    declarations: [
        GetStatusUserPipe
    ],
    exports: [
        GetStatusUserPipe
    ]
})

export class UsuarioEstatusPipeModule { }