export class AppConfig {

  URL_BASE_API: string;
  URL_AUTH_API: string;

  DATA_AUTH_CLIENTE: string;
  DATA_AUTH_GRANT_APP: string;

  TIEMPO_ACT_JOBS: string;
  TIEMPO_ACT_FECHA_SISTEMA: string;

}
