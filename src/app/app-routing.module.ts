import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from './utils/guard/auth.guard';
import { LoginComponent } from './pages/auth/login/login.component';
import { LoginModule } from './pages/auth/login/login.module';
import { LogoutComponent } from './pages/auth/logout/logout.component';
import { LogoutModule } from './pages/auth/logout/logout.module';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    //outlet: "home",
    loadChildren: () => import('../app/pages/pages.module')
      .then(m => m.PagesModule),
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'logout',
    component: LogoutComponent,
  },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: '**', redirectTo: 'inicio' },
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [
    RouterModule.forRoot(routes, config),
    LoginModule,
    LogoutModule
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
}