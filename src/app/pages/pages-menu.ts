import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  // {
  //   title: 'Menú de navegación',
  //   group: true,
  // },
  {
    title: 'Inicio',
    icon: 'home-outline',
    link: '/inicio',
    home: true,
  },
  {
    title: 'Monitoreo',
    icon: 'monitor-outline',
    link: '/monitoreo',
    home: true,
  },
  {
    title: 'Reportes',
    icon: 'file-outline',
    link: '/reporte',
    home: true,
  },
  {
    title: 'Ejecución administrativa',
    icon: 'settings-2-outline',
    link: '/ejecucionAdministrativa',
    home: true,
  },

  {
    title: 'Configuración',
    icon: 'options-2-outline',
   //home: true,
   children: [
     {
       title: 'Elementos',
       link: '/configuracion/elementos',
     },
    ],
  },
  {
    title: 'Seguridad',
    icon: 'shield-outline',
    children: [
      {
        title: 'Usuarios',
        link: '/Seguridad/Usuarios',
      },
      {
        title: 'Roles',
        link: '/Seguridad/Roles',
      },
      {
        title: 'Contrasena',
        link: '/Seguridad/Contrasenas',
      },
      {
        title: 'Estatus de usuarios',
        link: '/Seguridad/Estatus-Usuarios',
      },
    ],
  },
  {
   title: 'Mensajeria',
   icon: 'email-outline',
   children: [
     {
       title: 'SMS',
       link: '/Mensajeria/Sms',
     },
     {
       title: 'Correo',
       link: '/Mensajeria/Correo',
     },
    ],
  },
  {
    title: 'Perfil de usuario',
    icon: 'person-outline',
    link: '/perfil',
    home: true,
  },
  // {
  //   title: 'Auth',
  //   icon: 'lock-outline',
  //   children: [
  //     {
  //       title: 'Login',
  //       link: '/auth/login',
  //     },
  //     {
  //       title: 'Register',
  //       link: '/auth/register',
  //     },
  //     {
  //       title: 'Request Password',
  //       link: '/auth/request-password',
  //     },
  //     {
  //       title: 'Reset Password',
  //       link: '/auth/reset-password',
  //     },
  //   ],
  // },
];
