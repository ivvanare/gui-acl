import { Component } from '@angular/core';
import { ContentHeader } from '../components/content-header/content-header';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './inicio.component.html',
})
export class InicioComponent {
  
  titulo: string = "Inicio";
  breadcrumbs: ContentHeader[] = [
  	{active: true, display: this.titulo}
  ];
  
}
