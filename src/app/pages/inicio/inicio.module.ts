import { NgModule } from '@angular/core';
import { NbCardModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { InicioComponent } from './inicio.component';
import { ContentHeaderModule } from '../components/content-header/content-header.module';

@NgModule({
  imports: [
    NbCardModule,
    ThemeModule,
    ContentHeaderModule
  ],
  declarations: [
    InicioComponent,
  ],
})
export class InicioModule { }
