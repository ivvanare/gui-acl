import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { NbCardModule,NbButtonModule, NbInputModule, NbTabsetModule, NbIconModule } from '@nebular/theme';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ContentHeaderModule } from '../components/content-header/content-header.module';
import { ConfigContraseñaEstatusPipesModule } from '../../utils/pipe/config-contraseña-estatus/config-contraseña-estatus-pipes.module';
import { GetMensajePatronContrasenaPipeModule } from '../../utils/pipe/mensaje-patron-contraseña/msj-patron-contraseña-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    FormsModule,
    ReactiveFormsModule,
    NbTabsetModule,
    NbIconModule,
    ContentHeaderModule,
    ConfigContraseñaEstatusPipesModule,
    GetMensajePatronContrasenaPipeModule
  ],
  declarations: [ProfileComponent]
})
export class ProfileModule { 

}
