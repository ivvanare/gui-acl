import { Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import { FormControl,FormGroup, Validators, FormBuilder  } from '@angular/forms';
import { UsuarioService } from '../../services/usuario.service';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpEventType } from '@angular/common/http';

import { Usuario } from '../../models';
import { AuthService } from '../../services/auth/auth.service';
import { FormValidatorService } from '../../services/utils/form-validator.service';
import { AlertaService } from '../../services/utils/alerta.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ConstantesConfig } from '../../utils';
import { ContentHeader } from '../components/content-header/content-header';

// export function requiredFileType( type: string ) {
//   return function (control: FormControl) {
//     const file = control.value;
//     if ( file ) {
//       const extension = file.name.split('.')[1].toLowerCase();
//       if ( type.toLowerCase() !== extension.toLowerCase() ) {
//         return {
//           requiredFileType: true
//         };
//       }
      
//       return null;
//     }

//     return null;
//   };
// }

/**
  * Representa la clase contenedora de los métodos a ejecutar en las operaciones relacionadas a la
  * gestión del perfil de usuario
  */

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  titulo: string = "Perfil de usuario";
  breadcrumbs: ContentHeader[] = [
  	{active: false, display: 'Configuración', url: '/Configuracion/seguridad'},
  	{active: false, display: 'Seguridad', url: '/Configuracion/seguridad'},
  	{active: true, display: this.titulo}
  ];
  
  tiempo: number;

  usuario: Usuario = new Usuario();

  passUserForm: FormGroup;

  private obsParam$: Subject<void> = new Subject<void>();

  errores: string[] = [];

  imagenSeleccionada: File;

  progreso: number;

  urlImagenUsuario: any;

  mensajesRequeridos: string[] = [];

  /**
    * Método constructor de la clase
    */
  constructor(
    public authService: AuthService,
    public formValidatorService: FormValidatorService,
    private usuarioService: UsuarioService,
    private alertaService: AlertaService,
    private domSanitizer: DomSanitizer
  ) { }

  /**
    * Método que se ejecuta al momento de carga inicial de la clase.
    * Realiza la búsqueda de la información del usuario con la sesión actual en la aplicación
    */
  ngOnInit() {
    this.tiempo = window.performance.now();
    this.cargarUsuario();
    // console.log(this.usuario)
  }

  get f() { return this.passUserForm.controls; }

  /**
    * Método que se ejecuta al luego de la carga de la plantilla visual.
    * Realiza el cálculo del tiempo estimado para la carga de la plantilla
    */
  ngAfterViewInit() {
    this.tiempo = window.performance.now() - this.tiempo;
  }

  /**
    * Método que permite la búsqueda de la información del usuario con la sesión
    * activa en la aplicación, con el uso del servicio de autenticación. Permite la construcción del
    * grupo de validaciones para el formulario para cambio de contraseña con el uso del
    * servicio para validación de formularios. Se utiliza el servicio para configuración de contraseñas
    * para obtener las configuraciones habilitadas para el registro o actualización de una
    * contraseña
    */
  private cargarUsuario(): void {
    this.usuario = this.authService.usuario;
    this.formValidatorService.editPassUserFormValidators().then((data: FormGroup) => {
      this.passUserForm = data;
      this.mensajesRequeridos.push(this.formValidatorService.contrasenaValidators.filter(c => c.cod_requirement == ConstantesConfig.COD_CONTRASENA_MINLEGTH)[0].message);
      this.mensajesRequeridos.push(this.formValidatorService.contrasenaValidators.filter(c => c.cod_requirement == ConstantesConfig.COD_CONTRASENA_MAXLEGTH)[0].message);
    });

    this.obtenerImagen(this.usuario.avatar);
  }

  /**
    * Método que permite la actualización de la contraseña del usuario con sesión activa, a partir de
    * los datos obtenidos del formulario para contraseñas, con el uso de los servicios de autenticación
    * y de usuarios
    */
  updatePassword(): void {
    if (this.passUserForm.invalid) {
      return;
    }

    this.alertaService.doSwalQuestion().fire({
      title: this.titulo,
      text: `¿Está seguro de actualizar la contraseña para el usuario '${this.usuario.name}'?`
    }).then((result) => {
      if (result.value) {

        this.authService.checkCredenciales(this.usuario.email, this.passUserForm.value.old_password).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(
          json => {
            if (json.valid) {

              this.usuario = Object.assign(this.usuario, this.passUserForm.value as Usuario);

              this.usuarioService.update(this.usuario).pipe(
                takeUntil(this.obsParam$)
              ).subscribe(
                json => {
                  this.formValidatorService.editPassUserFormValidators().then((data: any) => {
                    this.passUserForm = data;
                    this.alertaService.doSwalSuccess({ title: 'Contraseña actualizada', text: `${json.message}: ${this.usuario.name}` });
                  });
                },
                err => {
                  this.toggleErroresBoxFromActions();
                  if (![null, undefined].includes(err.error.message)) {
                    this.errores = [];
                    this.errores.push(err.error.message);
                  } else {
                    this.errores = err.error.error as string[];
                  }
                }
              );

            } else {
              this.alertaService.doSwalError({ title: json.error, text: json.message });
            }
          },
          err => {
            this.toggleErroresBoxFromActions();
            this.errores = [];
            this.errores.push(err.error.message as string);
          }
        );

      }
    });
  }

  /**
    * Método que permite alterar el estilo del contenedor de mensajes para errores
    */
  toggleErroresBox(): void {
    document.getElementById('errores').toggleAttribute('hidden');
  }

  /**
    * Método que permite alterar el estilo del contenedor de mensajes para errores, a partir
    * de una acción realizada en la administración de procesos
    */
  toggleErroresBoxFromActions(): void {
    document.getElementById('errores').hasAttribute('hidden') ? document.getElementById('errores').toggleAttribute('hidden') : null;
  }

  /**
    * Método que permite identificar la selección de una imagen de usuario y establecer
    * caracteristicas de configuración
    */
  seleccionarImagen(event: any) {
    this.imagenSeleccionada = event.target.files[0];

    if (!this.imagenSeleccionada || this.imagenSeleccionada.type.indexOf("image") < 0) {
      this.alertaService.doSwalError({ title: this.titulo, text: `Debe seleccionar una archivo de tipo imagen` });
      this.imagenSeleccionada = null;
    } else if ((this.imagenSeleccionada.size / 1000) > 2048) {
      this.alertaService.doSwalError({ title: this.titulo, text: `El tamaño de imagen es excesivo` });
      this.imagenSeleccionada = null;
    }
  }

  /**
    * Método que permite ejecutar la carga de una imagen para el usuario con sesión activa, a partir
    * de la información relacionada a la imagen seleccionada previamente, con el uso del servicio de
    * usuarios
    */
  cargarImagen() {
    // console.log(this.usuario)
    this.alertaService.doSwalQuestion().fire({
      title: this.titulo,
      text: `¿Está seguro de actualizar la imagen para el usuario '${this.usuario.name}'?`
    }).then((result) => {
      if (result.value) {

        if (!this.imagenSeleccionada) {
          this.alertaService.doSwalError({ title: this.titulo, text: `Debe seleccionar una imagen previamente` });
        } else {
          this.usuarioService.cargarAvatar(this.imagenSeleccionada).subscribe(
            event => {
              if (event.type === HttpEventType.UploadProgress) {
                this.progreso = Math.round((event.loaded / event.total) * 100);
              } else if (event.type === HttpEventType.Response) {
                let response: any = event.body;
                this.usuario.avatar = response.user.avatar;
                this.obtenerImagen(this.usuario.avatar);
                this.progreso = 0;
                this.imagenSeleccionada = null;

                this.alertaService.doSwalSuccess({ title: 'Imagen actualizada', text: response.message });
              }
            },
            err => {
              this.toggleErroresBoxFromActions();
              this.errores = err.error.errors.avatar as string[];
              this.progreso = 0;
              this.imagenSeleccionada = null;
            }
          );
        }

      }
    });
  }

  /**
    * Método que permite obtener una imagen asociada a un usuario, a partir del nombre de la misma
    * obtenido como parámetro
    */
  private obtenerImagen(nombreImagen: string) {
    this.usuarioService.descargarAvatar(nombreImagen).pipe(
      takeUntil(this.obsParam$)
    ).subscribe(response => {
      this.urlImagenUsuario = this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(response));
    });
  }

  /**
    * Método que se ejecuta al momento de destrucción de la clase.
    */
  ngOnDestroy() {
    this.obsParam$.next();
    this.obsParam$.complete();
  }

}
