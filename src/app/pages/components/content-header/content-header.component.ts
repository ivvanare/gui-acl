import { Component, OnInit, Input } from '@angular/core';
import { ContentHeader } from './content-header';

@Component({
  selector: 'app-content-header',
  templateUrl: './content-header.component.html',
  styleUrls: ['./content-header.component.scss']
})

export class ContentHeaderComponent implements OnInit {

	@Input() titulo: string;
	@Input() breadcrumbs: ContentHeader[];

  constructor() { }

  ngOnInit() {
  }

}
