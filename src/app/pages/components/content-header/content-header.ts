export interface ContentHeader {
  active?: boolean;
  route?: string;
  display?: string;
  url?: string;
}
