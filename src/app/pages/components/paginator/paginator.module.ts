import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PaginatorRoutingModule } from "./paginator-routing.module";
import { PaginatorComponent } from "./paginator.component";
import { NbIconModule } from '@nebular/theme';

@NgModule({
  imports: [
    CommonModule,
    PaginatorRoutingModule,
    NbIconModule
  ],
  declarations: [
    PaginatorComponent
  ],
  exports: [
    PaginatorComponent
  ]
})
export class PaginatorModule {}
