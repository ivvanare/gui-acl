import { Component, OnInit, SimpleChanges, OnChanges, Input } from '@angular/core';

/**
  * Representa la clase contenedora de los métodos a ejecutar en las operaciones relacionadas a la
  * paginación de una lista informativa
  */
@Component({
  selector: 'paginator-nav',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit, OnChanges {

  @Input() paginador: any;

  @Input() links: string;

  @Input() parametros: string;

  paginas: number[];

  desde: number;
  hasta: number;

  /**
    * Método constructor de la clase
    */
  constructor() { }

  /**
    * Método que se ejecuta al momento de carga inicial de la clase.
    * Realiza la inicialización de las propiedades del paginador
    */
  ngOnInit() {
    this.initPaginator();
  }

  /**
    * Método que se ejecuta al momento de cambiar un elemento en el paginador, de forma que
    * se puedan ajustar las propiedades del mismo a los nuevos requerimientos del usuario
    */
  ngOnChanges(changes: SimpleChanges) {
    let paginadorNew = changes['paginador'];
    if (paginadorNew && paginadorNew.previousValue) {
      this.initPaginator();
    }
  }

  /**
    * Método que permite inicializar las propiedades del paginador, tomando en cuenta las
    * características disponibles en la lista informativa
    */
  private initPaginator(): void {
    console.log(this.paginador,this.paginador.number)
    this.desde = Math.min(Math.max(1,
      (this.paginador.number != undefined ? this.paginador.number : this.paginador.current_page) - 4),
      (this.paginador.totalPages != undefined ? this.paginador.totalPages : this.paginador.last_page) - 5);
    this.hasta = Math.max(Math.min(
      (this.paginador.totalPages != undefined ? this.paginador.totalPages : this.paginador.last_page),
      (this.paginador.number != undefined ? this.paginador.number : this.paginador.current_page) + 4), 6);

    if ((this.paginador.totalPages != undefined ? this.paginador.totalPages : this.paginador.last_page) > 5) {
      this.paginas = new Array(this.hasta - this.desde + 1).fill(0).map((_valor, indice) => indice + this.desde);
    } else {
      this.paginas = new Array(
        this.paginador.totalPages != undefined ? this.paginador.totalPages : this.paginador.last_page
      ).fill(0).map((_valor, indice) => indice + 1);
    }
  }

}
