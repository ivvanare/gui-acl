import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContentHeaderComponent } from './content-header/content-header.component';

const COMPONENTS = [
  ContentHeaderComponent
];

@NgModule({
  imports: [CommonModule],
  exports: [CommonModule, ...COMPONENTS],
  declarations: [
    ...COMPONENTS
  ],
})
export class ComponentsModule {
}
