import { Component, OnInit, OnDestroy } from '@angular/core';
import { Usuario } from '../../../models/usuario';
import { AuthService } from '../../../services/auth/auth.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';
import { FormValidatorService } from '../../../services/utils/form-validator.service';
import { AlertaService } from '../../../services/utils/alerta.service';
import {Location} from '@angular/common';

/**
  * Representa la clase contenedora de los métodos a ejecutar en el acceso del usuario a la aplicación
  */
@Component({
  selector: 'app-logourt',
  templateUrl: './logout.component.html',
})
export class LogoutComponent implements OnInit, OnDestroy {

  private obsParam$: Subject<void> = new Subject<void>();

  constructor(
    private alertaService: AlertaService,
    private router: Router,
    private authService: AuthService,
    private _location: Location
  ) { }

  ngOnInit(): void {
    this.logout();
  }


  /**
    * Método que permite ejecutar el cierre de la sesión a partir de los datos
    * del usuario actual, con el uso del servicio de autenticación
    */
   logout(): void {

    this.alertaService.doSwalQuestion().fire({
      title: "Cerrar sesión",
      text: `¿Está seguro de realizar el cierre de sesión?`,
    }).then((result) => {
      if (result.value) {

        this.authService.logout().pipe(
          takeUntil(this.obsParam$)
        ).subscribe(
          response => {
            this.alertaService.doSwalToast({ type: 'success', title: response.message });
            this.router.navigate(["/login"]);
          }
        );

      }else if (result.dismiss) {
        this._location.back();
      }
    });

  }

    /**
    * Método que se ejecuta al momento de destrucción de la clase.
    */
   ngOnDestroy() {
    this.obsParam$.next();
    this.obsParam$.complete();
  }
}
