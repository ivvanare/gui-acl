import { Component, OnInit, OnDestroy } from '@angular/core';
import { Usuario } from '../../../models/usuario';
import { AuthService } from '../../../services/auth/auth.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';
import { FormValidatorService } from '../../../services/utils/form-validator.service';
import { AlertaService } from '../../../services/utils/alerta.service';

/**
  * Representa la clase contenedora de los métodos a ejecutar en el acceso del usuario a la aplicación
  */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  titulo: string = 'Iniciar sesión';

  private obsParam$: Subject<void> = new Subject<void>();

  loginForm: FormGroup;
  errores: string;
  showMessages = {};
  errors = [];
  messages = [];
  submitted:boolean = false;
  redirectDelay = 5;

  /**
    * Método constructor de la clase
    */
  constructor(
    private authService: AuthService,
    private router: Router,
    private formValidatorService: FormValidatorService,
    private alertaService: AlertaService
  ) { }

  /**
    * Método que se ejecuta al momento de carga inicial de la clase. 
    * Verifica el estado de autenticación del usuario y establece las validaciones del formulario de acceso
    */
  ngOnInit() {
    
    if (this.authService.isAuthenticated()) {
      this.alertaService.doSwalToast({ icon: 'info', title: `Autenticado con el usuario ${this.authService.usuario.name}` });
      this.router.navigate(['/inicio']);
    }

    this.loginForm = this.formValidatorService.loginFormValidators();
  }

  get lf() { return this.loginForm.controls; }

  /**
    * Método que ejecuta el proceso de acceso a la aplicación, a partir de los datos obtenidos del
    * formulario de acceso, con el uso del servicio de autenticación
    */
  login() {

    if (this.loginForm.invalid) {
      return;
    }

    this.submitted = true;

    var _this = this;
    this.errors = [];
    this.messages = [];
    this.submitted = true;

    this.authService.login(this.loginForm.value as Usuario).pipe(
      takeUntil(this.obsParam$)
    ).subscribe(
      response => {
        this.submitted = false;
        this.router.navigate(['/inicio']);
        if (response.alertas_password && response.alertas_password.error) {
          setTimeout(() => {
            this.alertaService.doSwalToastConfirm().fire({
              type: 'warning',
              title: response.alertas_password.error,
              text: response.alertas_password.message
            }).then((result) => {
              if (result.value) {
                this.router.navigate(['/perfil-usuario']);
              }
            });
          }, 3000);
        } else {
          this.alertaService.doSwalToast({ 
            icon: 'success',
            title: `Bienvenido ${this.authService.usuario.name}`
          });
        }
      },
      err => {

        this.submitted = false;

        if (err.error.error && err.error.message) {
          this.alertaService.doSwalError({ title: err.error.error, text: err.error.message });
        } else if (err.status === 429) {
          this.alertaService.doSwalError({ title: 'Error', text: 'Se han realizado demasiadas peticiones al servidor. Intente mas tarde' });
        } else if (err.status === 0){
          this.alertaService.doSwalError({ 
            title: 'Error',
            text: 'ha ocurrido un error al conectar con el servidor. Por favor verifique la conexión e intente nuevamente' });
        }
        this.loginForm.patchValue({
          password: ''
        });
      }
    );
  }
  /**
  * Método que se ejecuta al momento de destrucción de la clase.
  */
  ngOnDestroy() {
    this.obsParam$.next();
    this.obsParam$.complete();
  }

}
