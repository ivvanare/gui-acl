import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';

import { InicioComponent } from './inicio/inicio.component';
import { ProfileComponent } from './profile/profile.component'


const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'inicio',
      component: InicioComponent,
    },
    {
      path: 'perfil',
      component: ProfileComponent
    },
    {
      path: 'Seguridad',
      loadChildren: () => import('./seguridad/seguridad.module')
        .then(m => m.SeguridadModule),
    },
    {
      path: '',
      redirectTo: 'inicio',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
