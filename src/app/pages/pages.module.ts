import { NgModule } from '@angular/core';

import { NbMenuModule } from '@nebular/theme';

import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';

import { InicioModule } from './inicio/inicio.module';
import { ProfileModule } from './profile/profile.module';
import { ThemeModule } from './@theme/theme.module';
import { RouterModule } from '@angular/router';
import { SeguridadModule } from './seguridad/seguridad.module';

@NgModule({
  imports: [
    RouterModule,
    ThemeModule,
    NbMenuModule,
    PagesRoutingModule,

    InicioModule,
    ProfileModule,
    SeguridadModule,
  ],
  declarations: [
    PagesComponent,
  ],
})
export class PagesModule {
}
