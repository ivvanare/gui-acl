import { Component, OnDestroy, OnInit } from '@angular/core';
import { 
  NbMediaBreakpointsService, 
  NbMenuService, 
  NbSidebarService, 
  NbThemeService 
} from '@nebular/theme';
import { AuthService  } from '../../../../services/auth/auth.service';
import { map, takeUntil, takeWhile } from 'rxjs/operators';
import { Subject, BehaviorSubject } from 'rxjs';
import { UsuarioService } from '../../../../services/usuario.service';
import { DomSanitizer } from '@angular/platform-browser';
import * as moment from 'moment';
import 'moment/min/locales';
import { AlertaService } from 'src/app/services/utils/alerta.service';
import { Router } from '@angular/router';
import { ParametroService } from 'src/app/services/parametro.service';
import { FechaService } from 'src/app/services/utils/fecha.service';
import { ConstantesConfig } from 'src/app/utils';
import { AppConfigService } from 'src/app/utils/config/app-config.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  private obsParam$: Subject<void> = new Subject<void>();

  userPictureOnly: boolean = false;
  user: any;
  urlImagenUsuario: any;
  horaDB: any;

  fechaSistema: string;
  fechaSistema2:any;

  private obsTimeoutS: BehaviorSubject<any>;
  enableRefreshSession: boolean = false;
  diff: number;

  private alive: boolean = true;
  userMenu = [ 
    {
      title: 'Perfil',
      icon: 'person-outline',
      link: ['/perfil'],
    },
    {
      title: 'Salir',
      icon: 'unlock-outline',
      link: ['/logout'],
    },
  ];

  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    // private userService: UserData,
    private breakpointService: NbMediaBreakpointsService,
    private authService: AuthService,
    private usuarioService: UsuarioService,
    private domSanitizer: DomSanitizer,
    private alertaService: AlertaService,
    private router: Router,
    private parametroService: ParametroService,
    private fechaService: FechaService,
    private appConfigService: AppConfigService,

  ) {
    this.obsTimeoutS = new BehaviorSubject<any>(ConstantesConfig.REFRESH_SYSTEM_TIME);
  }

  ngOnInit() {
    this.cargarUsuario();
    this.obtenerFechaSistema();
  }

  private cargarUsuario(): void {
    this.user = this.authService.usuario;
    //this.obtenerImagen(this.user.avatar);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.alive = false;
    this.obsTimeoutS.next(null);
    this.obsTimeoutS.complete();
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');

    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  /**
  * Método que permite realizar la búsqueda de la información en relación a las fechas de 
  * inicio y cierre para transmisión de archivos, con el uso del servicio de parámetros.
  * Se utiliza el servicio para configuraciones de la aplicación en la sincronización
  * de la fecha del sistema desde base de datos
  */
   obtenerFechaSistema(): void {
    this.usuarioService.getHoraDB().pipe(
      takeUntil(this.obsParam$)
    ).subscribe(response => {
      this.fechaSistema = this.fechaService.getDateInFormat(response, this.fechaService.DATE_FORMAT_HTML5_MOM) as string;
        this.obsTimeoutS.pipe(
          takeUntil(this.obsParam$)
        ).subscribe(_res => {

          setTimeout(() => {
            this.obtenerFechaSistema();
          }, Number(this.appConfigService.getConfig("TIEMPO_ACT_FECHA_SISTEMA")) * 1000);

          this.calcularFechaSistema();
        });
      

    });
  }

  /**
    * Método que permite realizar el cálculo automático de la fecha actual, a partir de
    * la fecha de la aplicación desde base de datos, con el uso del servicio de parámetros
    */
  calcularFechaSistema(): void {
    this.obsTimeoutS.pipe(
      takeUntil(this.obsParam$)
    ).subscribe(_res =>

      setTimeout(() => {
        this.fechaSistema = this.parametroService.calcularFechaSistema(this.fechaSistema);
        this.verificarParaRefreshToken();
        this.calcularFechaSistema();
      }, ConstantesConfig.REFRESH_SYSTEM_TIME)

    );
  }

  /**
    * Método que permite realizar la verificación de la fecha actual del sistema en comparación
    * a la fecha de expiración asociada al token de acceso, con el uso del servicio de
    * autenticación, a forma de establecer el estado de la sesión
    */
  private verificarParaRefreshToken() {

    this.parametroService.currentDate.pipe(
      takeUntil(this.obsParam$)
    ).subscribe(fecha => {
      let now: any = fecha;

      this.fechaSistema2 = moment(fecha).format("dddd, MMMM Do YYYY, h:mm:ss a")

      let payload: any = this.authService.obtenerPayload(this.authService.token, 1);

      if (payload) {
        this.diff = payload.exp - now.unix();
        if (this.diff <= 10) {

          this.authService.logout().pipe(
            takeUntil(this.obsParam$)
          ).subscribe(res => {
            this.alertaService.doSwalToast({ icon: 'info', title: `La sesión ha expirado. ${res.message}` });
            this.router.navigate(["/login"]);            
          });

        } else if (this.diff < 300) {
          this.enableRefreshSession = true;
        }
      }
    });

  }

  /**
    * Método que permite ejecutar el refrescamiento de la sesión a partir de los datos
    * del usuario actual, con el uso del servicio de autenticación, a forma de establecer 
    * el estado de la sesión
    */
  refreshSession(): void {
    this.authService.refreshSession().pipe(
      takeUntil(this.obsParam$)
    ).subscribe(_response => {
      this.enableRefreshSession = false;
      this.alertaService.doSwalToast({ icon: 'success', title: `La sesión del usuario ${this.authService.usuario.name} ha sido extendida con éxito` });
    });
  }

  
  /**
  * Método que permite obtener una imagen asociada a un usuario, a partir del nombre de la misma
  * obtenido como parámetro
  */
  /* private obtenerImagen(nombreImagen: string) {
    this.usuarioService.descargarAvatar(nombreImagen).pipe(
      takeUntil(this.obsParam$)
    ).subscribe(response => {
      this.urlImagenUsuario = this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(response));
    });
  }*/

}
