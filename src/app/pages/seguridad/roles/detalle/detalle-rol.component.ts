import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';

import { Rol } from '../../../../models/rol';
import { Permiso } from '../../../../models/permiso';
import { RolService } from '../../../../services/rol.service';
import { PermisoService } from '../../../../services/permiso.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DataTransferService } from "../../../../services/utils/data-transfer.service";
import { FormValidatorService } from '../../../../services/utils/form-validator.service';
import { AlertaService } from '../../../../services/utils/alerta.service';
import { ContentHeader } from '../../../components/content-header/content-header';

/**
  * Representa la clase contenedora de los métodos a ejecutar en las operaciones relacionadas a los
  * detalles para los roles de usuarios
  */
@Component({
  selector: 'app-detalle-rol',
  templateUrl: './detalle-rol.component.html'
})
export class DetalleRolComponent implements OnInit, AfterViewInit, OnDestroy {

  edit: boolean = false;
  accion:string;
  urlBack ='/Seguridad/Roles';

  titulo: string = 'Roles';
  breadcrumbs: ContentHeader[] = [
  	{active: false, display: 'Configuración', url: `${this.urlBack}`},
    {active: false, display: 'Seguridad', url: `${this.urlBack}`},
    {active: false, display: this.titulo, url: `${this.urlBack}` }
  ];
  
  private rol: Rol = new Rol();

  permisos: Array<Permiso>;

  tiempo: number;

  errores: any;

  private obsParam$: Subject<void> = new Subject<void>();

  roleForm: FormGroup;

  /**
    * Método constructor de la clase
    */
  constructor(
    private rolService: RolService,
    private activatedRoute: ActivatedRoute,
    private dataTransfer: DataTransferService,
    private router: Router,
    private permisoService: PermisoService,
    private formValidatorService: FormValidatorService,
    private alertaService: AlertaService
  ) { }

  /**
    * Método que se ejecuta al momento de carga inicial de la clase.
    * Realiza la búsqueda de la información relacionada a un rol, en conjunto con la lista de
    * permisos disponibles de forma general en la aplicación
    */
  ngOnInit() {
    this.tiempo = window.performance.now();
    this.cargarRol();

    this.permisoService.getList().pipe(
      takeUntil(this.obsParam$)
    ).subscribe((permisosList: any) => {
      this.permisos = permisosList as Permiso[];
    });
  }

  /**
    * Método que permite realizar la búsqueda de la información relacionada a un rol, a partir
    * de su identificador obtenido como parámetro desde la dirección URL, con el uso del
    * servicio de roles. Se realiza la búsqueda del grupo de validaciones para el formulario
    * de roles a partir del servicio para validaciones de formularios
    */
  cargarRol() {
    this.activatedRoute.params.pipe(
      takeUntil(this.obsParam$)
    ).subscribe(params => {
      let id = params['id'];

      this.roleForm = this.formValidatorService.roleFormValidators();

      if (id) {
        this.rolService.getById(id).subscribe(role => {
          this.rol = role.rol as Rol;

          this.roleForm.patchValue({
            name: this.rol.name,
            all_permissions: this.rol.all_permissions,
            permissions: this.rol.permissions
          });

          this.edit = true;
          this.breadcrumbs.push(
            {active: true, display: 'Editar'}
          )
        });
      } else {
        this.edit = false;
        this.breadcrumbs.push(
          {active: true, display: 'Crear'}
        )
      }
    })
  }

  get f() { return this.roleForm.controls; }

  /**
    * Método que se ejecuta al luego de la carga de la plantilla visual.
    * Realiza el cálculo del tiempo estimado para la carga de la plantilla
    */
  ngAfterViewInit() {
    this.tiempo = window.performance.now() - this.tiempo;
    this.dataTransfer.changeMessage(this.tiempo.toString());
  }

  /**
    * Método que permite crear un rol, a partir de los datos obtenidos del formulario de
    * roles, con el uso del servicio de roles
    */
  create(): void {
    if (this.roleForm.invalid) {
      return;
    }

    this.alertaService.doSwalQuestion().fire({
      title: 'Crear rol',
      text: `¿Está seguro de realizar la acción para el rol '${this.roleForm.value.name}'?`
    }).then((result) => {
      if (result.value) {

        this.rol = Object.assign(this.rol, this.roleForm.value as Rol);

        this.rolService.create(this.rol).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(
          json => {
            this.alertaService.doSwalSuccess({ title: 'Rol creado', text: `${json.message}: ${json.rol.name}` });
            this.router.navigate([`${this.urlBack}`]);
          },
          err => {
            this.errores = err.error.errors as string[];
          }
        );

      }
    });
  }

  /**
    * Método que permite actualizar un rol, a partir de los datos obtenidos del formulario de
    * roles, con el uso del servicio de roles
    */
  update(): void {
    if (this.roleForm.invalid) {
      return;
    }

    this.alertaService.doSwalQuestion().fire({
      title: 'Actualizar rol',
      text: `¿Está seguro de realizar la acción para el rol '${this.roleForm.value.name}'?`
    }).then((result) => {
      if (result.value) {

        this.rol = Object.assign(this.rol, this.roleForm.value as Rol);

        this.rolService.update(this.rol).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(
          json => {
            this.alertaService.doSwalSuccess({ title: 'Rol actualizado', text: `${json.message}: ${json.rol.name}` });
            this.router.navigate([`${this.urlBack}`]);
          },
          err => {
            this.errores = err.error.errors as any;
          }
        );

      }
    });

  }

  /**
    * Método que permite ajustar los valores de los permisos en el formulario de roles
    */
  allPermissions(): void {
    this.roleForm.patchValue({ all_permissions: "1" });
  }

  /**
    * Método que permite ajustar los valores de los permisos en el formulario de roles
    */
  notAllPermissions(): void {
    this.roleForm.patchValue({ all_permissions: "0" });
  }

  /**
    * Método que se ejecuta al momento de destrucción de la clase.
    */
  ngOnDestroy() {
    this.obsParam$.next();
    this.obsParam$.complete();
  }

}

