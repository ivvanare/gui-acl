import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { DataTransferService } from "../../../services/utils/data-transfer.service";
import { RolService } from '../../../services/rol.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { Rol } from '../../../models/rol';
import { AuthService } from '../../../services/auth/auth.service';
import { AlertaService } from '../../../services/utils/alerta.service';
import { TablaService } from '../../../services/utils/tabla.service';
import { ContentHeader } from '../../components/content-header/content-header';

/**
  * Representa la clase contenedora de los métodos a ejecutar en las operaciones relacionadas a los
  * roles de los usuarios
  */
@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html'
})
export class RolesComponent implements OnInit, AfterViewInit, OnDestroy {

  titulo: string = "Roles";
  breadcrumbs: ContentHeader[] = [
  	{active: false, display: 'Configuración', url: '/Seguridad/Roles'},
  	{active: false, display: 'Seguridad', url: '/Seguridad/Roles'},
  	{active: true, display: this.titulo}
  ];
  loading:boolean = true;

  roles: Rol[];

  tiempo: number;

  pag: any;

  links: string = "/Seguridad/Roles/Page";

  parametros: string;

  private obsParam$: Subject<void> = new Subject<void>();

  
  /**
    * Método constructor de la clase
    */
  constructor(
    private dataTransfer: DataTransferService,
    private rolService: RolService,
    private activatedRoute: ActivatedRoute,
    public authService: AuthService,
    private alertaService: AlertaService,
    public tablaService: TablaService,
    private router: Router
  ) { }

  /**
    * Método que se ejecuta al momento de carga inicial de la clase.
    * Realiza la búsqueda de la lista de roles
    */
  ngOnInit() {
    this.tiempo = window.performance.now();
    this.cargarRoles();
  }

  /**
    * Método que se ejecuta al luego de la carga de la plantilla visual.
    * Realiza el cálculo del tiempo estimado para la carga de la plantilla
    */
  ngAfterViewInit() {
    this.tiempo = window.performance.now() - this.tiempo;
    this.dataTransfer.changeMessage(this.tiempo.toString());
  }

  /**
    * Método que realiza la búsqueda de la lista de roles, de forma paginada a partir de la
    * página, columna y orden obtenidos desde la dirección URL como parámetros, con el uso del servicio
    * de roles
    */
  private cargarRoles() {
    this.loading = true;

    this.activatedRoute.paramMap.pipe(
      takeUntil(this.obsParam$)
    ).subscribe(params => {
      let page: number = +params.get("pagina");
      page = page ? page : 1;
      let col: string = params.get("columna");
      col = col ? col : 'name';
      let sort: string = params.get("orden");
      sort = sort ? sort : 'asc';

      this.rolService.getListPaginated(page, col, sort).pipe(
        takeUntil(this.obsParam$)
      ).subscribe(
        (response: any) => {
          if (response.data instanceof Array) {
            this.roles = response.data as Rol[];
          } else {
            this.roles = [];
            for (var i in response.data) {
              this.roles.push(response.data[i] as Rol);
            }
          }

          this.pag = response;
          this.parametros = `/${col}/${sort}`;

          if (this.roles.length > 0 && this.pag.last_page < page) {
            this.router.navigate([`${this.links}/${page}/${col}/${sort}`]);
          }
          this.loading = false;
        }
      );

    });
  }

  /**
   * Método que realiza la búsqueda de la lista de roles, aplicando filtros seleccionados,
   * de forma paginada a partir de la página, columna y orden obtenidos: el primero desde la
   * dirección URL, el segundo y tercero desde los valores seleccionados por el usuario, todos como parámetros
   */
  filtrarTabla() {
    this.loading = true;

    this.activatedRoute.paramMap.pipe(
      takeUntil(this.obsParam$)
    ).subscribe(params => {
      let page: number = +params.get("pagina");
      page = page ? page : 1;
      let col: string = this.tablaService.getPathValue();
      col = col ? col : 'name';
      let sort: string = this.tablaService.getSort(col);
      sort = sort ? sort : "asc";

      this.rolService.getListPaginated(page, col, sort).pipe(
        takeUntil(this.obsParam$)
      ).subscribe(
        response => {
          if (response.data instanceof Array) {
            this.roles = response.data as Rol[];
          } else {
            this.roles = [];
            for (var i in response.data) {
              this.roles.push(response.data[i] as Rol);
            }
          }

          this.pag = response;
          this.parametros = `/${col}/${sort}`;

          if (this.roles.length > 0 && this.pag.totalPages < page + 1) {
            this.router.navigate([`${this.links}/${page}/${col}/${sort}`]);
          }

          this.loading = false;
        }
      );

    });
  }

  /**
    * Método que se ejecuta al momento de destrucción de la clase.
    */
  ngOnDestroy() {
    this.obsParam$.next();
    this.obsParam$.complete();
  }

  /**
    * Método que permite ejecutar la activación de un rol, a partir de los datos pertenecientes al mismo,
    * con el uso del servicio de roles
    */
  activateRol(rol: Rol): void {
    this.alertaService.doSwalQuestion().fire({
      title: 'Activar rol',
      text: `¿Está seguro de realizar la acción para el rol '${rol.name}'?`
    }).then((result) => {
      if (result.value) {

        this.rolService.activate(rol.id).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(json => {

          this.alertaService.doSwalSuccess({ title: 'Rol activado', text: `${json.message}: ${json.rol.name}` });

          this.roles.forEach(rol => {
            if (rol.id == json.rol.id) {
              rol.status = json.rol.status;
            }
          });

        }, (error)=>{
          console.log(error);
          this.alertaService.doSwalError({title: 'El rol no pudo ser activado'})
        });

      }
    });
  }

  /**
    * Método que permite ejecutar la desactivación de un rol, a partir de los datos pertenecientes al mismo,
    * con el uso del servicio de roles
    */
  desactivateRol(rol: Rol): void {
    this.alertaService.doSwalQuestion().fire({
      title: 'Desactivar rol',
      text: `¿Está seguro de realizar la acción para el rol '${rol.name}'?`
    }).then((result) => {
      if (result.value) {

        this.rolService.desactivate(rol.id).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(json => {

          this.alertaService.doSwalSuccess({ title: 'Rol desactivado', text: `${json.message}: ${json.rol.name}` });

          this.roles.forEach(rol => {
            if (rol.id == json.rol.id) {
              rol.status = json.rol.status;
            }
          });

        }, (error)=>{
          console.log(error);
          this.alertaService.doSwalError({title: 'El rol no pudo ser actualizado'})
        });

      }
    });
  }


  /**
    * Método que permite la búsqueda de los permisos que se encuentra asociados a un rol, a partir de
    * los datos pertenecientes al mismo
    */
  showPermissions(rol: Rol): void {
    let permisosString: string = "<ul class='list-unstyled'>";

    if (rol.all_permissions == "0") {
      rol.permissions.forEach(per => {
        permisosString = permisosString + "<li>" + per.display_name + "</li>";
      });
    } else {
      permisosString = permisosString + "<li>Todos</li>";
    }
    permisosString = permisosString + "</ul>"

    this.alertaService.doSwalInformation({ title: `Permisos asignados a ${rol.name}`, html: permisosString });
  }

  /**
    * Método que permite ejecutar la eliminación de un rol, a partir de los datos pertenecientes al mismo,
    * con el uso del servicio de roles
    */
  deleteRol(rol: Rol): void {
    this.alertaService.doSwalQuestion().fire({
      title: 'Eliminar rol',
      text: `¿Está seguro de realizar la acción para el rol '${rol.name}'?`
    }).then((result) => {
      if (result.value) {

        this.rolService.delete(rol.id).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(json => {

          this.alertaService.doSwalSuccess({ title: 'Rol eliminado', text: `${json.message}: ${rol.name}` });

          this.roles = this.roles.filter(rolF => rolF.id != rol.id);

        }, (error)=>{
          console.log(error);
          this.alertaService.doSwalError({title: 'El rol no pudo ser eliminado'})
        });

      }
    });
  }

}

