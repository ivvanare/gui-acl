import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { RoleGuard } from '../../utils/guard/role.guard';
import { DetalleUsuarioComponent } from './usuarios/detalle/detalle-usuario.component';
//import { UsuariosModule } from './usuarios/usuarios.module';
import { RolesComponent } from './roles/roles.component';
import { DetalleRolComponent } from './roles/detalle/detalle-rol.component';
import { ContrasenasComponent } from './contrasenas/contrasenas.component';
import { DetalleContrasenaComponent } from './contrasenas/detalle/detalle-contrasena.component';
import { EstatusUsuariosComponent } from './estatus-usuarios/estatus-usuarios.component';
import { DetalleEstatusUsuarioComponent } from './estatus-usuarios/detalle/detalle-estatus-usuario.component';
import { EstatusUsuario } from '../../models';
import { SeguridadComponent } from './seguridad.component';

const routes: Routes = [{
  path: '',
  component: SeguridadComponent,
  children: [
    {
      path: 'Usuarios',
      component: UsuariosComponent,
      canActivate: [RoleGuard],
      data: { permissions: ['25-01-XX-01', '25-01-XX-04', '25-01-XX-05', '25-01-XX-06', '25-01-XX-07'] },
    },
    {
      path: 'Usuarios/Crear',
      component: DetalleUsuarioComponent,
      canActivate: [ RoleGuard],
      data: { permissions: ['25-01-XX-02'] }
    },
    {
      path: 'Usuarios/Editar/:id',
      component: DetalleUsuarioComponent,
      canActivate: [RoleGuard],
      data: { permissions: ['25-01-XX-03'] }
    },
    {
      path: 'Usuarios/page/:pagina/:columna/:orden',
      component: UsuariosComponent,
      canActivate: [RoleGuard],
      data: { permissions: ['25-01-XX-01', '25-01-XX-04', '25-01-XX-05', '25-01-XX-06', '25-01-XX-07'] }
    },
    {
      path: 'Roles',
      component:RolesComponent,
      canActivate: [RoleGuard],
      data: { permissions: ['25-02-XX-01', '25-02-XX-04', '25-02-XX-05', '25-02-XX-06', '25-02-XX-07'] },
    },
    {
      path: 'Roles/Editar/:id',
      component: DetalleRolComponent,
      canActivate: [RoleGuard],
      data: { permissions: ['25-02-XX-03'] }
    },
    {
      path: 'Roles/Crear',
      component: DetalleRolComponent,
      canActivate: [RoleGuard],
      data: { permissions: ['25-02-XX-02'] }
    },
    {
      path: 'Roles/page/:pagina/:columna/:orden',
      component:RolesComponent,
      canActivate: [RoleGuard],
      data: { permissions: ['25-02-XX-01', '25-02-XX-04', '25-02-XX-05', '25-02-XX-06', '25-02-XX-07'] }
    },
    {
      path: 'Contrasenas',
      component: ContrasenasComponent,
      canActivate: [RoleGuard],
      data: { permissions: ['25-03-XX-01', '25-03-XX-04', '25-03-XX-05', '25-03-XX-06'] },
    },
    {
      path: 'Contrasenas/Crear',
      component: DetalleContrasenaComponent,
      canActivate: [RoleGuard],
      data: { permissions: ['25-03-XX-02'] }
    },
    {
      path: 'Contrasenas/Editar/:id',
      component: DetalleContrasenaComponent,
      canActivate: [RoleGuard],
      data: { permissions: ['25-03-XX-03'] }
    },
    {
      path: 'Contrasenas/page/:pagina/:columna/:orden',
      component: ContrasenasComponent,
      canActivate: [RoleGuard],
      data: { permissions: ['25-03-XX-01', '25-03-XX-04', '25-03-XX-05', '25-03-XX-06'] }
    },
    {
      path: 'Estatus-Usuarios',
      component: EstatusUsuariosComponent,
      canActivate: [RoleGuard],
      data: { permissions: ['25-04-XX-01', '25-04-XX-04'] },
    },
    {
      path: 'Estatus-Usuarios/Crear',
      component: DetalleEstatusUsuarioComponent,
      canActivate: [RoleGuard],
      data: { permissions: ['25-04-XX-02'] }
    },
    {
      path: 'Estatus-Usuarios/Editar/:id',
      component: DetalleEstatusUsuarioComponent,
      canActivate: [RoleGuard],
      data: { permissions: ['25-04-XX-03'] }
    },
    {
      path: 'Estatus-Usuarios/page/:pagina/:columna/:orden',
      component: EstatusUsuario,
      canActivate: [RoleGuard],
      data: { permissions: ['25-04-XX-01', '25-04-XX-04'] }
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeguridadRoutingModule {
}


