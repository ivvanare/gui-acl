import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { DataTransferService } from "../../../services/utils/data-transfer.service";
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../services/auth/auth.service';
import { AlertaService } from '../../../services/utils/alerta.service';
import { TablaService } from '../../../services/utils/tabla.service';
import { Contrasena } from '../../../models/contrasena';
import { ContrasenaService } from '../../../services/contrasena.service';
import { ContentHeader } from '../../components/content-header/content-header';

/**
  * Representa la clase contenedora de los métodos a ejecutar en las operaciones relacionadas a los
  * roles de los usuarios
  */
@Component({
  selector: 'app-contrasenas',
  templateUrl: './contrasenas.component.html'
})
export class ContrasenasComponent implements OnInit, AfterViewInit, OnDestroy {

  titulo: string = "Contraseñas";
  breadcrumbs: ContentHeader[] = [
  	{active: false, display: 'Configuración', url: '/Seguridad/Contrasenas'},
  	{active: false, display: 'Seguridad', url: '/Seguridad/Contrasenas'},
  	{active: true, display: this.titulo}
  ];
  loading:boolean = true;

  configContrasenas: Contrasena[];

  tiempo: number;

  pag: any;

  links: string = "/Seguridad/Contrasenas/page";

  parametros: string;

  private obsParam$: Subject<void> = new Subject<void>();

  /**
    * Método constructor de la clase
    */
  constructor(
    private dataTransfer: DataTransferService,
    private contrasenaService: ContrasenaService,
    private activatedRoute: ActivatedRoute,
    public authService: AuthService,
    private alertaService: AlertaService,
    public tablaService: TablaService,
    private router: Router
  ) { }

  /**
    * Método que se ejecuta al momento de carga inicial de la clase.
    * Realiza la búsqueda de la lista de roles
    */
  ngOnInit() {
    this.tiempo = window.performance.now();
    this.cargarContrasenas();
  }

  /**
    * Método que se ejecuta al luego de la carga de la plantilla visual.
    * Realiza el cálculo del tiempo estimado para la carga de la plantilla
    */
  ngAfterViewInit() {
    this.tiempo = window.performance.now() - this.tiempo;
    this.dataTransfer.changeMessage(this.tiempo.toString());
  }

  /**
    * Método que realiza la búsqueda de la lista de roles, de forma paginada a partir de la
    * página, columna y orden obtenidos desde la dirección URL como parámetros, con el uso del servicio
    * de roles
    */
  private cargarContrasenas() {
    this.activatedRoute.paramMap.pipe(
      takeUntil(this.obsParam$)
    ).subscribe(params => {
      let page: number = +params.get("pagina");
      page = page ? page : 1;
      let col: string = params.get("columna");
      col = col ? col : 'id';
      let sort: string = params.get("orden");
      sort = sort ? sort : 'asc';

      this.contrasenaService.getListPaginated(page, col, sort).pipe(
        takeUntil(this.obsParam$)
      ).subscribe(
        (response: any) => {
          if (response.data instanceof Array) {
            this.configContrasenas = response.data as Contrasena[];
          } else {
            this.configContrasenas = [];
            for (var i in response.data) {
              this.configContrasenas.push(response.data[i] as Contrasena);
            }
          }

          this.pag = response;
          this.parametros = `/${col}/${sort}`;

          if (this.configContrasenas.length > 0 && this.pag.last_page < page) {
            this.router.navigate([`${this.links}/${page}/${col}/${sort}`]);
          }
          this.loading= false;
        }
      );

    });
  }

  /**
   * Método que realiza la búsqueda de la lista de roles, aplicando filtros seleccionados,
   * de forma paginada a partir de la página, columna y orden obtenidos: el primero desde la
   * dirección URL, el segundo y tercero desde los valores seleccionados por el usuario, todos como parámetros
   */
  filtrarTabla() {
    this.activatedRoute.paramMap.pipe(
      takeUntil(this.obsParam$)
    ).subscribe(params => {
      let page: number = +params.get("pagina");
      page = page ? page : 1;
      let col: string = this.tablaService.getPathValue();
      col = col ? col : 'id';
      let sort: string = this.tablaService.getSort(col);
      sort = sort ? sort : "asc";

      this.contrasenaService.getListPaginated(page, col, sort).pipe(
        takeUntil(this.obsParam$)
      ).subscribe(
        response => {
          if (response.data instanceof Array) {
            this.configContrasenas = response.data as Contrasena[];
          } else {
            this.configContrasenas = [];
            for (var i in response.data) {
              this.configContrasenas.push(response.data[i] as Contrasena);
            }
          }

          this.pag = response;
          this.parametros = `/${col}/${sort}`;

          if (this.configContrasenas.length > 0 && this.pag.totalPages < page + 1) {
            this.router.navigate([`${this.links}/${page}/${col}/${sort}`]);
          }
        }
      ), (error)=>{
        console.log(error);
        this.alertaService.doSwalError({title: 'El listado no pudo ser filtrado'})
      };

    });
  }

  /**
    * Método que se ejecuta al momento de destrucción de la clase.
    */
  ngOnDestroy() {
    this.obsParam$.next();
    this.obsParam$.complete();
  }

  /**
    * Método que permite ejecutar la activación de una configuración de contraseña, a partir de
    * los datos pertenecientes al mismo, con el uso del servicio de roles
    */
  activateConfigContrasena(contrasena: Contrasena): void {
    this.alertaService.doSwalQuestion().fire({
      title: 'Activar configuración',
      text: `¿Está seguro de realizar la acción para la configuración '${contrasena.name}'?`
    }).then((result) => {
      if (result.value) {

        this.contrasenaService.activate(contrasena.id).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(json => {
          this.alertaService.doSwalSuccess({ title: 'Configuración activada', text: `${json.message}: ${json.contrasena.name}` });

          let indexConfig = this.configContrasenas.findIndex((conf: Contrasena) => conf.id == json.contrasena.id);
          this.configContrasenas[indexConfig].status = json.contrasena.status;
        }, (error)=>{
          console.log(error);
          this.alertaService.doSwalError({title: 'La configuración no pudo ser activada'})
        });

      }
    });
  }

  /**
    * Método que permite ejecutar la desactivación de una configuración de contraseña, a
    * partir de los datos pertenecientes al mismo, con el uso del servicio de roles
    */
  deactivateConfigContrasena(contrasena: Contrasena): void {
    this.alertaService.doSwalQuestion().fire({
      title: 'Desactivar configuración',
      text: `¿Está seguro de realizar la acción para la configuración '${contrasena.name}'?`
    }).then((result) => {
      if (result.value) {

        this.contrasenaService.deactivate(contrasena.id).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(json => {
          this.alertaService.doSwalSuccess({ title: 'Configuración desactivada', text: `${json.message}: ${json.contrasena.name}` });

          let indexConfig = this.configContrasenas.findIndex((conf: Contrasena) => conf.id == json.contrasena.id);
          this.configContrasenas[indexConfig].status = json.contrasena.status;
        }, (error)=>{
          console.log(error);
          this.alertaService.doSwalError({title: 'La configuración no pudo ser desactivada'})
        });

      }
    });
  }

  /**
    * Método que permite ejecutar la eliminación de un rol, a partir de los datos pertenecientes al mismo,
    * con el uso del servicio de roles
    */
  deleteConfigContrasena(contrasena: Contrasena): void {
    this.alertaService.doSwalQuestion().fire({
      title: 'Eliminar configuración',
      text: `¿Está seguro de realizar la acción para la configuración '${contrasena.name}'?`
    }).then((result) => {
      if (result.value) {

        this.contrasenaService.delete(contrasena.id).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(json => {
          this.alertaService.doSwalSuccess({ title: 'Configuración eliminada', text: `${json.message}: ${contrasena.name}` });

          this.configContrasenas = this.configContrasenas.filter(conf => conf.id != contrasena.id);
        }, (error)=>{
          console.log(error);
          this.alertaService.doSwalError({title: 'La configuración no pudo ser eliminada'})
        });

      }
    });
  }

}

