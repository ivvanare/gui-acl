import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataTransferService } from "../../../../services/utils/data-transfer.service";
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';
import { FormValidatorService } from '../../../../services/utils/form-validator.service';
import { AlertaService } from '../../../../services/utils/alerta.service';
import { Contrasena } from '../../../../models';
import { ContrasenaService } from '../../../../services/contrasena.service';
import { ContentHeader } from '../../../components/content-header/content-header';

/**
  * Representa la clase contenedora de los métodos a ejecutar en las operaciones relacionadas a los
  * detalles para las configuraciones de contraseñas
  */
@Component({
  selector: 'app-detalle-contrasena',
  templateUrl: './detalle-contrasena.component.html'
})
export class DetalleContrasenaComponent implements OnInit, AfterViewInit, OnDestroy {

  edit: boolean = false;
  accion:string;
  urlBack ='/Seguridad/Contrasenas';

  titulo: string = "Contraseñas";
  breadcrumbs: ContentHeader[] = [
  	{active: false, display: 'Configuración', url: `${this.urlBack}`},
    {active: false, display: 'Seguridad', url: `${this.urlBack}`},
    {active: false, display: this.titulo, url: `${this.urlBack}` }
  ];
  
  contrasena: Contrasena = new Contrasena();

  tiempo: number;

  errores: any;

  private obsParam$: Subject<void> = new Subject<void>();

  configForm: FormGroup;

  /**
    * Método constructor de la clase
    */
  constructor(
    private activatedRoute: ActivatedRoute,
    private dataTransfer: DataTransferService,
    private router: Router,
    private formValidatorService: FormValidatorService,
    private alertaService: AlertaService,
    private contrasenaService: ContrasenaService
  ) { }

  /**
    * Método que se ejecuta al momento de carga inicial de la clase.
    * Realiza la búsqueda de la información relacionada a una configuracion de contraseña
    */
  ngOnInit() {
    this.tiempo = window.performance.now();
    this.cargarConfigContrasena();
  }

  /**
    * Método que permite realizar la búsqueda de la información relacionada a una configuración de contraseña, a partir
    * de su identificador obtenido como parámetro desde la dirección URL, con el uso del
    * servicio de configuración de contraseñas. Se realiza la búsqueda del grupo de validaciones para el formulario
    * de  configuración de contraseñas a partir del servicio para validaciones de formularios
    */
  cargarConfigContrasena() {
    this.activatedRoute.params.pipe(
      takeUntil(this.obsParam$)
    ).subscribe(params => {
      let id = params['id'];

      this.configForm = this.formValidatorService.editConfigContrasenaFormValidators();

      if (id) {
        this.contrasenaService.getById(id).subscribe(conf => {
          this.contrasena = conf.contrasena as Contrasena;

          this.configForm.patchValue({
            name: this.contrasena.name,
            description: this.contrasena.description,
            value: this.contrasena.value,
            message: this.contrasena.message,
            cod_requirement: this.contrasena.cod_requirement
          });

          this.edit = true;
          this.breadcrumbs.push(
            {active: true, display: 'Editar'}
          )
        });
      } else {
        this.edit = false;
        this.breadcrumbs.push(
          {active: true, display: 'Crear'}
        )
      }
    })
  }

  get f() { return this.configForm.controls; }

  /**
    * Método que se ejecuta al luego de la carga de la plantilla visual.
    * Realiza el cálculo del tiempo estimado para la carga de la plantilla
    */
  ngAfterViewInit() {
    this.tiempo = window.performance.now() - this.tiempo;
    this.dataTransfer.changeMessage(this.tiempo.toString());
  }

  /**
    * Método que permite crear una configuración de contraseña, a partir de los datos obtenidos del formulario de
    * configuración de contraseñas, con el uso del servicio de configuración de contraseñas
    */
  create(): void {
    if (this.configForm.invalid) {
      return;
    }

    this.alertaService.doSwalQuestion().fire({
      title: 'Crear configuración',
      text: `¿Está seguro de realizar la acción para la configuración '${this.configForm.value.name}'?`
    }).then((result) => {
      if (result.value) {

        this.contrasena = Object.assign(this.contrasena, this.configForm.value as Contrasena);

        this.contrasenaService.create(this.contrasena).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(
          json => {
            this.alertaService.doSwalSuccess({ title: 'Configuración creada', text: `${json.message}: ${json.contrasena.name}` });
            this.router.navigate([`${this.urlBack}`]);
          },
          err => {
            this.errores = err.error.errors as string[];
          }
        );

      }
    });
  }

  /**
    * Método que permite actualizar una configuración de contraseña, a partir de los datos obtenidos del formulario de
    * configuración de contraseñaS, con el uso del servicio de configuración de contraseñas
    */
  update(): void {
    if (this.configForm.invalid) {
      return;
    }

    this.alertaService.doSwalQuestion().fire({
      title: 'Actualizar configuración',
      text: `¿Está seguro de realizar la acción para la configuración '${this.configForm.value.name}'?`
    }).then((result) => {
      if (result.value) {

        this.contrasena = Object.assign(this.contrasena, this.configForm.value as Contrasena);

        this.contrasenaService.update(this.contrasena).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(
          json => {
            this.alertaService.doSwalSuccess({ title: 'Configuración actualizada', text: `${json.message}: ${json.contrasena.name}` });
            this.router.navigate([`${this.urlBack}`]);
          },
          err => {
            this.errores = err.error.errors as any;
          }
        );

      }
    });

  }

  /**
    * Método que permite ajustar los valores de estatus a activo en el formulario de configuración
    * de contraseñas
    */
  activeConfig(): void {
    this.configForm.patchValue({ status: "1" });
  }

  /**
    * Método que permite ajustar los valores de estatus a inactivo en el formulario de configuración
    * de contraseñas
    */
  inactiveConfig(): void {
    this.configForm.patchValue({ status: "0" });
  }

  /**
    * Método que se ejecuta al momento de destrucción de la clase.
    */
  ngOnDestroy() {
    this.obsParam$.next();
    this.obsParam$.complete();
  }

}

