import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';
import { FormValidatorService } from '../../../../services/utils/form-validator.service';
import { AlertaService } from '../../../../services/utils/alerta.service';
import { EstatusUsuario } from '../../../../models/estatus-usuario';
import { EstatusUsuarioService } from '../../../../services/estatus-usuario.service';
import { DataTransferService } from '../../../../services/utils/data-transfer.service';
import { ContentHeader } from '../../../components/content-header/content-header';

/**
  * Representa la clase contenedora de los métodos a ejecutar en las operaciones relacionadas al detalle
  * de los estatus para los usuarios de la aplicación
  */
@Component({
  selector: 'app-detalle-estatus-usuario',
  templateUrl: './detalle-estatus-usuario.component.html'
})
export class DetalleEstatusUsuarioComponent implements OnInit, AfterViewInit, OnDestroy {

  titulo: string = "Estatus de usuarios";
  edit: boolean = false;
  accion:string;
  urlBack ='/Seguridad/Estatus-Usuarios';

  breadcrumbs: ContentHeader[] = [
    {active: false, display: 'Seguridad', url: `${this.urlBack}`},
    {active: false, display: this.titulo, url: `${this.urlBack}`}
  ];
  
  tiempo: number;

  errores: any;

  private obsParam$: Subject<void> = new Subject<void>();

  userEstatusForm: FormGroup;

  private estatus = new EstatusUsuario();

  /**
   * Método constructor de la clase
   */
  constructor(
    private activatedRoute: ActivatedRoute,
    private dataTransfer: DataTransferService,
    private router: Router,
    private formValidatorService: FormValidatorService,
    private alertaService: AlertaService,
    private estatusService: EstatusUsuarioService
  ) { }

  /**
    * Método que se ejecuta al momento de carga inicial de la clase.
    * Realiza la búsqueda de la información de un estatus para un usuario
    */
  ngOnInit() {
    this.tiempo = window.performance.now();
    this.cargarEstatusUsuario();
  }

  get f() { return this.userEstatusForm.controls; }

  /**
    * Realiza la búsqueda de la información de un estatus para un usuario, a partir de su identificador, obtenido
    * como parámetro, con el uso del servicio de estatus para usuarios. Construye el grupo de validaciones
    * del formulario de estatus para usuarios con el uso del servicio para validación de formularios
    */
  private cargarEstatusUsuario() {
    this.activatedRoute.params.pipe(
      takeUntil(this.obsParam$)
    ).subscribe(params => {
      let id = params['id'];

      this.userEstatusForm = this.formValidatorService.userEstatusFormValidators();

      if (id) {
        this.estatusService.getById(id).subscribe(response => {
          this.estatus = response.estatus as EstatusUsuario;

          this.userEstatusForm.patchValue({
            cod_status: this.estatus.cod_status,
            descripcion: this.estatus.descripcion,
            usa_fechas: this.estatus.usa_fechas,
            valor_status: this.estatus.valor_status
          });

          this.edit = true;
          this.breadcrumbs.push(
            {active: true, display: 'Editar'}
          )
        });
      } else {
        this.edit = false;
        this.breadcrumbs.push(
          {active: true, display: 'Crear'}
        )
      }
    })
  }

  /**
    * Método que se ejecuta al luego de la carga de la plantilla visual.
    * Realiza el cálculo del tiempo estimado para la carga de la plantilla
    */
  ngAfterViewInit() {
    this.tiempo = window.performance.now() - this.tiempo;
    this.dataTransfer.changeMessage(this.tiempo.toString());
  }

  /**
    * Método que permite crear un estatus para un usuario, a partir de los datos obtenidos del formulario de
    * estatus para usuarios, con el uso del servicio de estatus para usuarios
    */
  create(): void {
    if (this.userEstatusForm.invalid) {
      return;
    }

    this.alertaService.doSwalQuestion().fire({
      title: 'Crear estatus de usuario',
      text: `¿Está seguro de realizar la acción para el estatus '${this.userEstatusForm.value.cod_status}'?`
    }).then((result) => {
      if (result.value) {

        this.estatus = Object.assign(this.estatus, this.userEstatusForm.value as EstatusUsuario);

        this.estatusService.create(this.estatus).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(
          json => {
            this.alertaService.doSwalSuccess({ title: 'Estatus de usuario creado', text: `${json.message}: ${json.estatus.cod_status}` });
            this.router.navigate([`${this.urlBack}`]);
          },
          err => {
            this.errores = err.error.errors as string[];
          }
        );

      }
    });
  }

  /**
    * Método que permite actualizar un estatus de usuario, a partir de los datos obtenidos del formulario de
    * estatus para usuarios, con el uso del servicio de estatus para usuarios
    */
  update(): void {
    if (this.userEstatusForm.invalid) {
      return;
    }

    this.alertaService.doSwalQuestion().fire({
      title: 'Actualizar estatus de usuario',
      text: `¿Está seguro de realizar la acción para el estatus '${this.userEstatusForm.value.cod_status}'?`
    }).then((result) => {
      if (result.value) {

        this.estatus = Object.assign(this.estatus, this.userEstatusForm.value as EstatusUsuario);

        this.estatusService.update(this.estatus).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(
          json => {
            this.alertaService.doSwalSuccess({ title: 'Estatus de usuario actualizado', text: `${json.message}: ${json.estatus.cod_status}` });
            this.router.navigate([`${this.urlBack}`]);
          },
          err => {
            this.errores = err.error.errors as any;
          }
        );

      }
    });

  }

  /**
    * Método que permite ajustar los valores del uso de fechas en el formulario de estatus para usuarios
    */
  useDates(): void {
    this.userEstatusForm.patchValue({ usa_fechas: "1" });
  }

  /**
    * Método que permite ajustar los valores del uso de fechas en el formulario de estatus para usuarios
    */
  dontUseDates(): void {
    this.userEstatusForm.patchValue({ usa_fechas: "0" });
  }

  /**
    * Método que permite ajustar los valores del estatus base en el formulario de estatus para usuarios
    */
  activeStatus(): void {
    this.userEstatusForm.patchValue({ valor_status: "1" });
  }

  /**
    * Método que permite ajustar los valores del estatus base en el formulario de estatus para usuarios
    */
  notActiveStatus(): void {
    this.userEstatusForm.patchValue({ valor_status: "0" });
  }

  /**
    * Método que se ejecuta al momento de destrucción de la clase.
    */
  ngOnDestroy() {
    this.obsParam$.next();
    this.obsParam$.complete();
  }

}
