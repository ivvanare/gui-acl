import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { EstatusUsuariosComponent } from './estatus-usuarios.component';


const routes: Routes = [
  { path: "", component: EstatusUsuariosComponent },

];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class EstatusUsuariosRoutingModule { }
