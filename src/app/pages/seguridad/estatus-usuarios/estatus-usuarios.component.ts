import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { DataTransferService } from "../../../services/utils/data-transfer.service";
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { EstatusUsuario } from '../../../models/estatus-usuario';
import { EstatusUsuarioService } from '../../../services/estatus-usuario.service';
import { AuthService } from '../../../services/auth/auth.service';
import { AlertaService } from '../../../services/utils/alerta.service';
import { TablaService } from '../../../services/utils/tabla.service';
import { ContentHeader } from '../../components/content-header/content-header';

/**
  * Representa la clase contenedora de los métodos a ejecutar en las operaciones relacionadas a los
  * estados de los usuarios en la aplicación
  */
@Component({
  selector: 'app-estatus-usuarios',
  templateUrl: './estatus-usuarios.component.html'
})
export class EstatusUsuariosComponent implements OnInit, AfterViewInit, OnDestroy {

  titulo: string = "Estatus de usuarios";
  breadcrumbs: ContentHeader[] = [
  	{active: false, display: 'Configuración', url: '/Seguridad/Estatus-Usuarios'},
  	{active: false, display: 'Seguridad', url: '/Seguridad/Estatus-Usuarios'},
  	{active: true, display: this.titulo}
  ];
  loading:boolean = true;
  
  estatusUser: EstatusUsuario[];

  tiempo: number;

  pag: any;

  links: string = "/seguridad/estatus-usuarios/page";

  parametros: string;

  private obsParam$: Subject<void> = new Subject<void>();

  /**
    * Método constructor de la clase
    */
  constructor(
    private dataTransfer: DataTransferService,
    private estatusUsuarioService: EstatusUsuarioService,
    private activatedRoute: ActivatedRoute,
    public authService: AuthService,
    private alertaService: AlertaService,
    public tablaService: TablaService,
    private router: Router
  ) { }

  /**
    * Método que se ejecuta al momento de carga inicial de la clase.
    * Realiza la búsqueda de la lista de estatus para usuarios
    */
  ngOnInit() {
    this.tiempo = window.performance.now();
    this.cargarEstatusUsuarios();
  }

  /**
    * Método que se ejecuta al luego de la carga de la plantilla visual.
    * Realiza el cálculo del tiempo estimado para la carga de la plantilla
    */
  ngAfterViewInit() {
    this.tiempo = window.performance.now() - this.tiempo;
    this.dataTransfer.changeMessage(this.tiempo.toString());
  }

  /**
    * Método que realiza la búsqueda de la lista de estatus para usuarios, de forma paginada a partir de la
    * página, columna y orden obtenidos desde la dirección URL como parámetros, con el uso del servicio
    * de usuarios
    */
  private cargarEstatusUsuarios() {
    this.activatedRoute.paramMap.pipe(
      takeUntil(this.obsParam$)
    ).subscribe(params => {
      let page: number = +params.get("pagina");
      page = page ? page : 1;
      let col: string = params.get("columna");
      col = col ? col : 'cod_status';
      let sort: string = params.get("orden");
      sort = sort ? sort : 'asc';

      this.estatusUsuarioService.getListPaginated(page, col, sort).pipe(
        takeUntil(this.obsParam$)
      ).subscribe(
        (response: any) => {
          if (response.data instanceof Array) {
            this.estatusUser = response.data as EstatusUsuario[];
          } else {
            this.estatusUser = [];
            for (var i in response.data) {
              this.estatusUser.push(response.data[i] as EstatusUsuario);
            }
          }

          this.pag = response;
          this.parametros = `/${col}/${sort}`;

          if (this.estatusUser.length > 0 && this.pag.totalPages < page + 1) {
            this.router.navigate([`${this.links}/${page}/${col}/${sort}`]);
          }
          this.loading = false;
        }
      );

    });
  }

  /**
   * Método que realiza la búsqueda de la lista de estatus para usuarios, aplicando filtros seleccionados,
   * de forma paginada a partir de la página, columna y orden obtenidos: el primero desde la
   * dirección URL, el segundo y tercero desde los valores seleccionados por el usuario, todos como parámetros,
   * con el uso del servicio de estatus para usuarios
   */
  filtrarTabla() {
    this.activatedRoute.paramMap.pipe(
      takeUntil(this.obsParam$)
    ).subscribe(params => {
      let page: number = +params.get("pagina");
      page = page ? page : 1;
      let col: string = this.tablaService.getPathValue();
      col = col ? col : 'cod_status';
      let sort: string = this.tablaService.getSort(col);
      sort = sort ? sort : "asc";

      this.estatusUsuarioService.getListPaginated(page, col, sort).pipe(
        takeUntil(this.obsParam$)
      ).subscribe(
        response => {
          if (response.data instanceof Array) {
            this.estatusUser = response.data as EstatusUsuario[];
          } else {
            this.estatusUser = [];
            for (var i in response.data) {
              this.estatusUser.push(response.data[i] as EstatusUsuario);
            }
          }

          this.pag = response;
          this.parametros = `/${col}/${sort}`;

          if (this.estatusUser.length > 0 && this.pag.totalPages < page + 1) {
            this.router.navigate([`${this.links}/${page}/${col}/${sort}`]);
          }
        }
      );

    });
  }

  /**
    * Método que se ejecuta al momento de destrucción de la clase.
    */
  ngOnDestroy() {
    this.obsParam$.next();
    this.obsParam$.complete();
  }

  /**
    * Método que permite eliminar un usuario, a partir de los datos pertenecientes al mismo,
    * con el uso del servicio de estatus para usuarios
    */
  deleteEstatusUsuario(estatus: EstatusUsuario): void {
    this.alertaService.doSwalQuestion().fire({
      title: 'Eliminar estatus de usuario',
      text: `¿Está seguro de realizar la acción para el estatus '${estatus.cod_status}'?`
    }).then((result) => {
      if (result.value) {

        this.estatusUsuarioService.delete(estatus.id).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(json => {
          this.alertaService.doSwalSuccess({ title: 'Estatus de usuario eliminado', text: `${json.message}: ${estatus.cod_status}` });
          this.estatusUser = this.estatusUser.filter(userF => userF.id != estatus.id);
        }, (error)=>{
          console.log(error);
          this.alertaService.doSwalError({title: 'El estatus de usuario no pudo ser eliminado'})
        });

      }
    });
  }

}
