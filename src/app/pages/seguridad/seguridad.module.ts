import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDialogModule,
  NbInputModule,
  NbPopoverModule,
  NbSelectModule,
  NbTabsetModule,
  NbTooltipModule,
  NbTableModule,
  NbIconModule,
  NbSpinnerModule,
  NbAlertModule,
  NbCalendarRangeModule,
} from '@nebular/theme';

// modules
import { ThemeModule } from '../../pages/@theme/theme.module';

// components
import { UsuariosComponent } from './usuarios/usuarios.component';
import { RolesComponent } from './roles/roles.component';
import { ContrasenasComponent } from './contrasenas/contrasenas.component';
import { DetalleUsuarioComponent } from './usuarios/detalle/detalle-usuario.component';
import { DetalleRolComponent } from './roles/detalle/detalle-rol.component';
import { DetalleContrasenaComponent } from './contrasenas/detalle/detalle-contrasena.component';
import { DetalleEstatusUsuarioComponent } from './estatus-usuarios/detalle/detalle-estatus-usuario.component';
import { EstatusUsuariosComponent } from './estatus-usuarios/estatus-usuarios.component';
import { SeguridadComponent } from './seguridad.component';

import { NgSelectModule } from '@ng-select/ng-select';
import { ContentHeaderModule } from '../components/content-header/content-header.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { GetMensajePatronContrasenaPipeModule } from '../../utils/pipe/mensaje-patron-contraseña/msj-patron-contraseña-pipes.module';
import { ConfigContraseñaEstatusPipesModule } from '../../utils/pipe/config-contraseña-estatus/config-contraseña-estatus-pipes.module';
import { StatusDetalleUserPipeModule } from '../../utils/pipe/estatus-detalle-user-pipe/usuario-detalle-estatus.module';
import { StatusUsoFechasPipeModule } from '../../utils/pipe/estatus-uso-fechas/estatus-uso-fechas.module';
import { EstatusUsuarioComponent } from './usuarios/estatus/estatus-usuario.component';
import { RolEstatusPipeModule } from '../../utils/pipe/rol-estatus/rol-estatus.module';
import { UsuarioEstatusPipeModule } from '../../utils/pipe/usuario-estatus/usuario-estatus.module';

import { SeguridadRoutingModule } from './seguridad-routing.module';
import { PaginatorModule } from '../components/paginator/paginator.module';


const COMPONENTS = [
  UsuariosComponent,
  RolesComponent,
  ContrasenasComponent,
  DetalleUsuarioComponent,
  DetalleRolComponent,
  DetalleContrasenaComponent,
  DetalleEstatusUsuarioComponent,
  EstatusUsuariosComponent,
  EstatusUsuarioComponent
];

const PIPES = [
  StatusDetalleUserPipeModule,
  GetMensajePatronContrasenaPipeModule,
  ConfigContraseñaEstatusPipesModule,
  StatusUsoFechasPipeModule,
  RolEstatusPipeModule,
  UsuarioEstatusPipeModule
];

const ENTRY_COMPONENTS = [
  EstatusUsuarioComponent
];

const MODULES = [

  CommonModule,
  PaginatorModule,
  NgSelectModule,
  ReactiveFormsModule,
  ContentHeaderModule,
  NbTableModule,
  NbIconModule,
  NbSpinnerModule,
  NbAlertModule,
  SeguridadRoutingModule,
  RouterModule,
  NbCalendarRangeModule,
  NbSelectModule,
  FormsModule,
  ThemeModule,
  NbDialogModule.forChild(),
  NbCardModule,
  NbCheckboxModule,
  NbTabsetModule,
  NbPopoverModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbTooltipModule,
];

const SERVICES = [
];

@NgModule({
  imports: [
    ...MODULES,
    ...PIPES,
  ],
  declarations: [
    ...COMPONENTS,
    SeguridadComponent
  ],
  providers: [
    ...SERVICES,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})

export class SeguridadModule {
}
