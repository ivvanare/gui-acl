import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Rol, Usuario } from '../../../../models';
import { Subject } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { UsuarioService } from '../../../../services/usuario.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DataTransferService } from '../../../../services/utils/data-transfer.service';
import { RolService } from '../../../../services/rol.service';
import { FormValidatorService } from '../../../../services/utils/form-validator.service';
import { AlertaService } from '../../../../services/utils/alerta.service';
import { takeUntil } from 'rxjs/operators';
import { ConstantesConfig } from '../../../../utils';
import { ContentHeader } from '../../../components/content-header/content-header';

/**
  * Representa la clase contenedora de los métodos a ejecutar en las operaciones relacionadas al detalle
  * de los usuarios de la aplicación
  */
@Component({
  selector: 'app-detalle-usuario',
  templateUrl: './detalle-usuario.component.html',
  styleUrls:['./detalle-usuario.component.scss']
})
export class DetalleUsuarioComponent implements OnInit, AfterViewInit, OnDestroy {

  titulo: string = "Usuarios";
  accion:string;
  urlBack ='/Seguridad/Usuarios';

  breadcrumbs: ContentHeader[] = [
  	{active: false, display: 'Configuración', url: `${this.urlBack}`},
    {active: false, display: 'Seguridad', url: `${this.urlBack}`},
    {active: false, display: this.titulo, url: `${this.urlBack}` }
  ];
  
  roles: Array<Rol>;

  tiempo: number;

  errores: any;

  private obsParam$: Subject<void> = new Subject<void>();

  userForm: FormGroup;

  private usuario = new Usuario();

  edit: boolean = false;

  mensajesRequeridos: string[] = [];

  /**
   * Método constructor de la clase
   */
  constructor(
    private usuarioService: UsuarioService,
    private activatedRoute: ActivatedRoute,
    private dataTransfer: DataTransferService,
    private router: Router,
    private rolService: RolService,
    public formValidatorService: FormValidatorService,
    private alertaService: AlertaService
  ) { }

  /**
    * Método que se ejecuta al momento de carga inicial de la clase.
    * Realiza la búsqueda de la información de un usuario. Permite la búsqueda de la
    * lista de roles generales
    */
  ngOnInit() {
    this.tiempo = window.performance.now();
    this.cargarUsuario();

    this.rolService.getList().pipe(
      takeUntil(this.obsParam$)
    ).subscribe((rolesList: any) => {
      this.roles = rolesList.data.roles as Rol[];
    });
  }

  get f() { return this.userForm.controls; }

  /**
    * Realiza la búsqueda de la información de un usuario, a partir de su identificador, obtenido
    * como parámetro, con el uso del servicio de usuarios. Construye el grupo de validaciones
    * del formulario de usuarios con el uso del servicio para validación de formularios
    */
  private cargarUsuario() {
    this.activatedRoute.params.pipe(
      takeUntil(this.obsParam$)
    ).subscribe(params => {
      let id = params['id'];
      if (id) {
        this.usuarioService.getById(id).subscribe(user => {
          this.usuario = user.user as Usuario;

          this.userForm = this.formValidatorService.editUserFormValidators();

          this.userForm.patchValue({
            name: this.usuario.name,
            email: this.usuario.email,
            roles: this.usuario.roles
          });

          this.edit = true;
          this.breadcrumbs.push(
            {active: true, display: 'Editar'}
          )
        });

      } else {
        this.formValidatorService.createUserFormValidators().then((data: FormGroup) => {
          this.userForm = data;
          this.mensajesRequeridos.push(this.formValidatorService.contrasenaValidators.filter(c => c.cod_requirement == ConstantesConfig.COD_CONTRASENA_MINLEGTH)[0].message);
          this.mensajesRequeridos.push(this.formValidatorService.contrasenaValidators.filter(c => c.cod_requirement == ConstantesConfig.COD_CONTRASENA_MAXLEGTH)[0].message);
        });

        this.edit = false;
        this.breadcrumbs.push(
          {active: true, display: 'Crear'}
        )
      }
    })
  }

  /**
    * Método que se ejecuta al luego de la carga de la plantilla visual.
    * Realiza el cálculo del tiempo estimado para la carga de la plantilla
    */
  ngAfterViewInit() {
    this.tiempo = window.performance.now() - this.tiempo;
    this.dataTransfer.changeMessage(this.tiempo.toString());
  }

  /**
    * Método que permite crear un usuario, a partir de los datos obtenidos del formulario de
    * usuarios, con el uso del servicio de usuarios
    */
  create(): void {
    if (this.userForm.invalid) {
      return;
    }

    this.alertaService.doSwalQuestion().fire({
      title: 'Crear usuario',
      text: `¿Está seguro de realizar la acción para el usuario '${this.userForm.value.name}'?`
    }).then((result) => {
      if (result.value) {

        this.usuario = Object.assign(this.usuario, this.userForm.value as Usuario);

        this.usuarioService.create(this.usuario).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(
          json => {
            this.alertaService.doSwalSuccess({ title: 'Usuario creado', text: `${json.message}: ${json.user.name}` });
            this.router.navigate([`${this.urlBack}`]);
          },
          err => {
            this.errores = err.error.errors as string[];
          }
        );

      }
    });
  }

  /**
    * Método que permite actualizar un usuario, a partir de los datos obtenidos del formulario de
    * usuarios, con el uso del servicio de usuarios
    */
  update(): void {
    if (this.userForm.invalid) {
      return;
    }

    this.alertaService.doSwalQuestion().fire({
      title: 'Actualizar usuario',
      text: `¿Está seguro de realizar la acción para el usuario '${this.userForm.value.name}'?`
    }).then((result) => {
      if (result.value) {

        this.usuario = Object.assign(this.usuario, this.userForm.value as Usuario);

        this.usuarioService.update(this.usuario).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(
          json => {
            this.alertaService.doSwalSuccess({ title: 'Usuario actualizado', text: `${json.message}: ${json.user.name}` });
            this.router.navigate([`${this.urlBack}`]);
            console.log(json)
          },
          err => {
            this.errores = err.error.errors as any;
          }
        );

      }
    });

  }

  /**
    * Método que se ejecuta al momento de destrucción de la clase.
    */
  ngOnDestroy() {
    this.obsParam$.next();
    this.obsParam$.complete();
  }

}
