import { Component, OnInit, AfterViewInit, OnDestroy, TemplateRef } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { Usuario, EstatusUsuario } from '../../../models';
import { DataTransferService } from '../../../services/utils/data-transfer.service';
import { UsuarioService } from '../../../services/usuario.service';
import { AuthService } from '../../../services/auth/auth.service';
import { AlertaService } from '../../../services/utils/alerta.service';
import { TablaService } from '../../../services/utils/tabla.service';
import { ModalService } from '../../../services/utils/modal.service';
import { EstatusUsuarioService } from '../../../services/estatus-usuario.service';
import { ContentHeader } from '../../components/content-header/content-header';
import { NbDialogService } from '@nebular/theme';
import { EstatusUsuarioComponent } from './estatus/estatus-usuario.component';

/**
  * Representa la clase contenedora de los métodos a ejecutar en las operaciones relacionadas a los
  * usuarios de la aplicación
  */
@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit, AfterViewInit, OnDestroy {

  titulo: string = "Usuarios";
  breadcrumbs: ContentHeader[] = [
  	{active: false, display: 'Configuración', url: '/Seguridad/Usuarios'},
  	{active: false, display: 'Seguridad', url: '/Seguridad/Usuarios'},
  	{active: true, display: this.titulo}
  ];
  
  usuarios: Usuario[];

  tiempo: number;

  pag: any;

  links: string = "/Seguridad/Usuarios/page";

  parametros: string;

  private obsParam$: Subject<void> = new Subject<void>();

  userSelected: Usuario;

  estatusSelected: number;

  listaEstatus: EstatusUsuario[];

  loading:boolean = true;

  /**
    * Método constructor de la clase
    */
  constructor(
    private dataTransfer: DataTransferService,
    private usuarioService: UsuarioService,
    private activatedRoute: ActivatedRoute,
    public authService: AuthService,
    private alertaService: AlertaService,
    public tablaService: TablaService,
    private router: Router,
    public modalService: ModalService,
    private estatusUsuarioService: EstatusUsuarioService,
    private dialogService: NbDialogService
  ) { }

  /**
    * Método que se ejecuta al momento de carga inicial de la clase.
    * Realiza la búsqueda de la lista de usuarios
    */
  ngOnInit() {
    this.tiempo = window.performance.now();
    this.estatusUsuarioService.getList().subscribe(response => {
      this.listaEstatus = response as EstatusUsuario[];
      this.cargarUsuarios();
    });
  }

  /**
    * Método que se ejecuta al luego de la carga de la plantilla visual.
    * Realiza el cálculo del tiempo estimado para la carga de la plantilla
    */
  ngAfterViewInit() {
    this.tiempo = window.performance.now() - this.tiempo;
    this.dataTransfer.changeMessage(this.tiempo.toString());
  }

  /**
    * Método que realiza la búsqueda de la lista de usuarios, de forma paginada a partir de la
    * página, columna y orden obtenidos desde la dirección URL como parámetros, con el uso del servicio
    * de usuarios
    */
  private cargarUsuarios() {
    this.loading = true;

    this.activatedRoute.paramMap.pipe(
      takeUntil(this.obsParam$)
    ).subscribe(params => {
      let page: number = +params.get("pagina");
      page = page ? page : 1;
      let col: string = params.get("columna");
      col = col ? col : 'id';
      let sort: string = params.get("orden");
      sort = sort ? sort : 'asc';

      this.usuarioService.getListPaginated(page, col, sort).pipe(
        takeUntil(this.obsParam$)
      ).subscribe(
        (response: any) => {
          this.loading = false;
          if (response.data instanceof Array) {
            this.usuarios = response.data as Usuario[];
          } else {
            this.usuarios = [];
            for (var i in response.data) {
              this.usuarios.push(response.data[i] as Usuario);
            }
          }

          this.pag = response;
          this.parametros = `/${col}/${sort}`;

          if (this.usuarios.length > 0 && this.pag.totalPages < page + 1) {
            this.router.navigate([`${this.links}/${page}/${col}/${sort}`]);
          }
        }
      );

    });
  }

  /**
   * Método que realiza la búsqueda de la lista de roles, aplicando filtros seleccionados,
   * de forma paginada a partir de la página, columna y orden obtenidos: el primero desde la
   * dirección URL, el segundo y tercero desde los valores seleccionados por el usuario, todos como parámetros
   */
  filtrarTabla() {
    this.activatedRoute.paramMap.pipe(
      takeUntil(this.obsParam$)
    ).subscribe(params => {
      let page: number = +params.get("pagina");
      page = page ? page : 1;
      let col: string = this.tablaService.getPathValue();
      col = col ? col : 'name';
      let sort: string = this.tablaService.getSort(col);
      sort = sort ? sort : "asc";

      this.usuarioService.getListPaginated(page, col, sort).pipe(
        takeUntil(this.obsParam$)
      ).subscribe(
        response => {
          if (response.data instanceof Array) {
            this.usuarios = response.data as Usuario[];
          } else {
            this.usuarios = [];
            for (var i in response.data) {
              this.usuarios.push(response.data[i] as Usuario);
            }
          }

          this.pag = response;
          this.parametros = `/${col}/${sort}`;

          if (this.usuarios.length > 0 && this.pag.totalPages < page + 1) {
            this.router.navigate([`${this.links}/${page}/${col}/${sort}`]);
          }
        }
      );

    });
  }

  /**
    * Método que permite activar un usuario, a partir de los datos pertenecientes al mismo,
    * con el uso del servicio de usuarios, tomando en cuenta la información especificada en la
    * ventana de estatus de usuarios
    */
  activateUser(usuario: Usuario): void {
    if (usuario.id == this.authService.usuario.id) {
      this.alertaService.doSwalError({ title: 'Activar usuario', text: `No es posible realizar la acción para el usuario ${usuario.name}` });
    }

    this.alertaService.doSwalQuestion().fire({
      title: 'Activar usuario',
      text: `¿Está seguro de realizar la acción para el usuario '${usuario.name}'?`
    }).then((result) => {
      if (result.value) {
        this.abrirVentanaModal(usuario, 1);
        this.dialogService.open(EstatusUsuarioComponent, {
          context: {
            usuario: usuario, 
            tipoEstatus: 1,
          },
        });
        this.dataTransfer.currentUserData.subscribe((response: Usuario) => {
          if (response) {
            let indexUser = this.usuarios.findIndex((user: Usuario) => user.id == response.id);
            this.usuarios[indexUser] = Object.assign(this.usuarios[indexUser], response);
          }
        }, (error)=>{
          console.log(error);
          this.alertaService.doSwalError({title: 'El usuario no pudo ser activado'})
        })
      }
    });
  }

  /**
    * Método que permite desactivar un usuario, a partir de los datos pertenecientes al mismo,
    * con el uso del servicio de usuarios, tomando en cuenta la información especificada en la
    * ventana de estatus de usuarios
    */
  deactivateUser(usuario: Usuario): void {
    if (usuario.id == this.authService.usuario.id) {
      this.alertaService.doSwalError({ title: 'Desactivar usuario', text: `No es posible realizar la acción para el usuario ${usuario.name}` });
    }

    this.alertaService.doSwalQuestion().fire({
      title: 'Desactivar usuario',
      text: `¿Está seguro de realizar la acción para el usuario '${usuario.name}'?`
    }).then((result) => {
      if (result.value) {
        this.abrirVentanaModal(usuario, 0);
        this.dataTransfer.currentUserData.subscribe((response: Usuario) => {
          if (response) {
            let indexUser = this.usuarios.findIndex((user: Usuario) => user.id == response.id);
            this.usuarios[indexUser] = Object.assign(this.usuarios[indexUser], response);
          }
        }, (error)=>{
          console.log(error);
          this.alertaService.doSwalError({title: 'El usuario no pudo ser desactivado'})
        })
      }
    });
  }

  /**
    * Método que permite realizar la búsqueda de los roles asociados a un usuario, a partir de
    * los datos obtenidos de este mediante parámetros
    */
  showRoles(usuario: Usuario): void {
    let rolesString: string = "<ul class='list-unstyled'>";

    usuario.roles.forEach(rol => {
      rolesString = rolesString + "<li>" + rol.name + "</li>";
    });

    rolesString = rolesString + "</ul>";

    this.alertaService.doSwalInformation({ title: `Roles asignados a ${usuario.name}`, html: rolesString });
  }

  /**
    * Método que permite eliminar un usuario, a partir de los datos pertenecientes al mismo,
    * con el uso del servicio de usuarios
    */
  deleteUser(usuario: Usuario): void {
    if (usuario.id == this.authService.usuario.id) {
      this.alertaService.doSwalError({ title: 'Eliminar usuario', text: `No es posible realizar la acción para el usuario ${usuario.name}` });
    }

    this.alertaService.doSwalQuestion().fire({
      title: 'Eliminar usuario',
      text: `¿Está seguro de realizar la acción para el usuario '${usuario.name}'?`
    }).then((result) => {
      if (result.value) {

        this.usuarioService.delete(usuario.id).pipe(
          takeUntil(this.obsParam$)
        ).subscribe(json => {
          this.alertaService.doSwalSuccess({ title: 'Usuario eliminado', text: `${json.message}: ${usuario.name}` });
          this.usuarios = this.usuarios.filter(userF => userF.id != usuario.id);
        }, (error)=>{
          console.log(error);
          this.alertaService.doSwalError({title: 'El usuario no pudo ser eliminado'})
        });

      }
    });
  }

  /**
    * Método que ejecuta la apertura de la ventana de modal para la identificación de la razón para
    * realizar una acción sobre el estatus del usuario, con los identificadores obtenidos como parámetros, con el uso
    * del servicio para manejo de ventanas modal
    */
  abrirVentanaModal(usuario: Usuario, estatus: number) {
    this.userSelected = usuario;
    this.estatusSelected = estatus;
    this.dialogService.open(EstatusUsuarioComponent, {
      context: {
        usuario: usuario,
        tipoEstatus: estatus
      },
    });
    //this.modalService.abrirModal();
  }

  /**
    * Método que se ejecuta al momento de destrucción de la clase.
    */
  ngOnDestroy() {
    this.obsParam$.next();
    this.obsParam$.complete();
  }

}
