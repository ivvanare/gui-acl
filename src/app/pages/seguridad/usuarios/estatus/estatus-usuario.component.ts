import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Usuario, EstatusUsuario } from '../../../../models';
import { FormGroup } from '@angular/forms';
import { ModalService } from '../../../../services/utils/modal.service';
import { AlertaService } from '../../../../services/utils/alerta.service';
import { EstatusUsuarioService } from '../../../../services/estatus-usuario.service';
import { FormValidatorService } from '../../../../services/utils/form-validator.service';
import { UsuarioService } from '../../../../services/usuario.service';
import { DataTransferService } from '../../../../services/utils/data-transfer.service';
import { NbDialogRef } from '@nebular/theme';
import { NbCalendarRange, NbDateService } from '@nebular/theme';

/**
  * Representa la clase contenedora de los métodos a ejecutar en las operaciones relacionadas a la
  * selección de la razón en una acción sobre el estatus de un usuario
  */
@Component({
  selector: 'app-estatus-usuario',
  templateUrl: './estatus-usuario.component.html'
})
export class EstatusUsuarioComponent implements OnInit, OnDestroy {

  @Input() usuario: Usuario;

  @Input() tipoEstatus: number;

  @Input() title: any;

  private obsParam$: Subject<void> = new Subject<void>();

  estatusUser: EstatusUsuario[];

  estatusForm: FormGroup;

  range: NbCalendarRange<Date>;

  /**
    * Método constructor de la clase
    */
  constructor(
    public modalService: ModalService,
    private alertaService: AlertaService,
    private estatusService: EstatusUsuarioService,
    private formValidatorService: FormValidatorService,
    private usuarioService: UsuarioService,
    private dataTransferService: DataTransferService,
    protected dialogRef: NbDialogRef<EstatusUsuarioComponent>,
    protected dateService: NbDateService<Date>
  ) {
    this.range = {
      start: this.dateService.addDay(this.monthStart, 3),
      end: this.dateService.addDay(this.monthEnd, -3),
    };

   }

  /**
    * Método que se ejecuta al momento de carga inicial de la clase
    */
  ngOnInit() {
    this.cargarSeleccionEstatus();
  }

  get f() { return this.estatusForm.controls; }

  /**
    * Permite la búsqueda de la información relacionada a la lista de estatus para la
    * activación o desactivación de un usuario, a partir de la selección de una de las acciones
    * especificadas
    */
  private cargarSeleccionEstatus() {
    this.estatusService.getList().pipe(
      takeUntil(this.obsParam$)
    ).subscribe(response => {
      this.estatusUser = response as EstatusUsuario[];
      this.estatusUser = this.estatusUser.filter(f => f.valor_status == this.tipoEstatus);
      this.estatusForm = this.formValidatorService.associateUserEstatusFormValidators();
      
        this.estatusForm.patchValue({
          fe_status_desde: this.range.start,
          fe_status_hasta: this.range.end
        });
      
      this.dataTransferService.currentDateRange.pipe(
        takeUntil(this.obsParam$)
      ).subscribe(message => {
        let fechas = message as Array<string>;
        if (fechas) {
          this.estatusForm.patchValue({
            fe_status_desde: fechas[0],
            fe_status_hasta: fechas[1]
          });
        }
      });
    });
  }

  /**
    * Método que permite ejecutar la desactivación de un usuario, a partir de los datos seleccionados
    * en el formulario para estatus de usuarios y la propia información del usuario
    */
  executeDeactivation() {
    if (this.estatusForm.value.estatus && this.estatusForm.value.estatus.usa_fechas == 0) {
      this.estatusForm.patchValue({
        fe_status_desde: null,
        fe_status_hasta: null
      });
    }else{
      this.estatusForm.patchValue({
        fe_status_desde: this.range.start,
        fe_status_hasta: this.range.end
      });
    }

    this.usuarioService.deactivate(this.usuario.id, this.estatusForm.value).pipe(
      takeUntil(this.obsParam$)
    ).subscribe(json => {
      this.cerrarVentanaModal();
      this.dataTransferService.changeUserInfo(json.user as Usuario);
      this.alertaService.doSwalSuccess({ title: `${json.success}`, text: `${json.message}: ${json.user.id}` });
    });
  }

  /**
    * Método que permite ejecutar la activación de un usuario, a partir de los datos seleccionados
    * en el formulario para estatus de usuarios y la propia información del usuario
    */
  executeActivation() {
    if (this.estatusForm.value.estatus && this.estatusForm.value.estatus.usa_fechas == 0) {
      this.estatusForm.patchValue({
        fe_status_desde: null,
        fe_status_hasta: null
      });
    }else{
      this.estatusForm.patchValue({
        fe_status_desde: this.range.start,
        fe_status_hasta: this.range.end
      });
    }

    this.usuarioService.activate(this.usuario.id, this.estatusForm.value).pipe(
      takeUntil(this.obsParam$)
    ).subscribe(json => {
      console.log(json)
      this.cerrarVentanaModal();
      this.dataTransferService.changeUserInfo(json.user as Usuario);
      this.alertaService.doSwalSuccess({ title: `${json.success}`, text: `${json.message}: ${json.user.id}` });
    });
    console.log(this.estatusForm.value)
  }

  /**
    * Método que ejecuta el cierre de la ventana de modal para la asociación de un estatus descriptivo
    * a un usuario de la aplicación
    */
  cerrarVentanaModal() {
    //this.modalService.cerrarModal();
    this.dialogRef.close();
    this.dataTransferService.changeDateRange(null);
    this.dataTransferService.changeUserInfo(null);
    this.ngOnDestroy();
  }

  /**
    * Método que se ejecuta al momento de destrucción de la clase.
    */
  ngOnDestroy() {
    this.obsParam$.next();
    this.obsParam$.complete();
  }

  get monthStart(): Date {
    return this.dateService.getMonthStart(new Date());
  }

  get monthEnd(): Date {
    return this.dateService.getMonthEnd(new Date());
  }
}

